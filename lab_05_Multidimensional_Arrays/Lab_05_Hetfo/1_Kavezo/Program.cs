﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Kavezo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(">> Italok bekérése");
            string[,] italokNaponta;
            italokNaponta = EladottItalokBekerese();

            Console.Clear();
            Console.WriteLine(">> Feladatok: \n");

            /* Adott nap italai */
            Console.Write("Hanyadik nap eladásait szeretnéd listázni? ");
            int napIndex = int.Parse(Console.ReadLine()) - 1;
            string[] adottNapiItalok = AdottNapotKivalogat(italokNaponta, napIndex);
            Console.WriteLine("\n{0}. napi italok:", napIndex + 1);
            for (int i = 0; i < adottNapiItalok.Length; i++)
                Console.WriteLine(adottNapiItalok[i]);


            /* Rendezés */
            NapiItallistaRendez(adottNapiItalok);


            /* IsmetlodesekKiszurese */
            string[] ismetlodesekNelkul = IsmetlodesekKiszurese(adottNapiItalok);
            Console.WriteLine("\n{0}. napi italok ismétlődések nélkül:", napIndex + 1);
            for (int i = 0; i < ismetlodesekNelkul.Length; i++)
                Console.WriteLine(ismetlodesekNelkul[i]);


            /* Italok */
            string[] osszesItal = Italok(italokNaponta);
            Console.WriteLine("\nEladott italok:");
            for (int i = 0; i < osszesItal.Length; i++)
                Console.WriteLine(osszesItal[i]);


            /* EladottMennyisegek */
            int[] mennyisegek = EladottMennyisegek(italokNaponta, osszesItal);
            Console.WriteLine("\nItalok + mennyiségek:");
            for (int i = 0; i < mennyisegek.Length; i++)
                Console.WriteLine(osszesItal[i] + ": " + mennyisegek[i] + " darab");



            Console.ReadLine();
        }

        static int BekerNapokSzama()
        {
            int napokSzama;
            do
            {
                Console.Write("Hány nap eladásait szeretnéd rögzíteni? ");
                napokSzama = int.Parse(Console.ReadLine());
            } while (napokSzama < 2);
            return napokSzama;
        }

        static string[,] EladottItalokBekerese()
        {
            int napokSzama = BekerNapokSzama(); // sorok száma a 2D-s tömbben
            const int MAX_ELADASOK_SZAMA_NAPONTA = 10; // oszlopok száma a 2D-s tömbben
            string[,] italokNaponta = new string[napokSzama, MAX_ELADASOK_SZAMA_NAPONTA];
            for (int i = 0; i < napokSzama; i++)
            {
                Console.WriteLine("{0}. napi italok (max 10 darab, ha kevesebb, üss üres entert)", i + 1);
                string ital;
                int napiDarabszam = 0;
                do
                {
                    ital = Console.ReadLine(); // a ReadLine() üres enter lenyomására üres stringgel "" tér vissza
                    if (ital != "")
                    {
                        italokNaponta[i, napiDarabszam] = ital;
                        napiDarabszam++;
                    }
                } while (ital != "" && napiDarabszam < MAX_ELADASOK_SZAMA_NAPONTA);
            }

            return italokNaponta;
        }

        static int AdottNapiItalokSzama(string[,] italokNaponta, int napIndex)
        {
            int db = 0;
            while (db < italokNaponta.GetLength(1) && italokNaponta[napIndex, db] != null)
            {
                db++;
            }
            return db;
        }

        static string[] AdottNapotKivalogat(string[,] italokNaponta, int napIndex)
        {
            // először megvizsgáljuk, hány elemű tömbre lesz szükségünk
            // majd a bemeneti tömb megfelelő sorából átmásolunk annyi elemet a kimeneti tömbbe
            int db = AdottNapiItalokSzama(italokNaponta, napIndex);
            string[] adottNapiItalok = new string[db];
            for (int i = 0; i < db; i++)
            {
                adottNapiItalok[i] = italokNaponta[napIndex, i];
            }
            return adottNapiItalok;
        }

        static bool StringKisebbE(string elso, string masodik)
        {
            int j = 0;
            while (j < elso.Length && j < masodik.Length && elso[j] == masodik[j])
            {
                j++;
            }
            if (j >= elso.Length && j < masodik.Length)
                return true;
            if (j >= masodik.Length)
                return false;
            if (elso[j] < masodik[j])
                return true;
            return false;
        }

        static void Csere(ref string elso, ref string masodik)
        {
            string seged = elso;
            elso = masodik;
            masodik = seged;
        }

        static void NapiItallistaRendez(string[] italok)
        {
            // Minimumkiválasztásos rendezés
            for (int i = 0; i < italok.Length - 1; i++)
            {
                int minIndex = i;
                for (int j = i + 1; j < italok.Length; j++)
                {
                    if (StringKisebbE(italok[j], italok[minIndex]))
                        minIndex = j;
                }
                Csere(ref italok[minIndex], ref italok[i]);
            }
        }

        static string[] IsmetlodesekKiszurese(string[] italok)
        {
            // helyben (bemeneti tömbben) szűr, azaz a tömb elejére pakolja a különbözőket, mert ha volt ismétlődés, a kevesebb elemet utaána úgyis át kell pakolni egy másik tömbbe (így megspórolunk egy tömblétrehozást)

            int utolsoIndexe = 0; // az utolsó különböző elem helyét jelöli a tömbben, ha eddig az indexig nézzük a tömb elemeit, akkor nincs / nem lesz köztük ismétlődés
            for (int i = 1; i < italok.Length; i++)
            {
                if (italok[i] != italok[utolsoIndexe])
                {
                    utolsoIndexe++;
                    italok[utolsoIndexe] = italok[i];
                }
            }
            if (utolsoIndexe + 1 == italok.Length) // ha nem volt ismétlődés
                return italok;
            else
            {
                string[] ujItalok = new string[utolsoIndexe + 1];
                for (int i = 0; i < ujItalok.Length; i++)
                {
                    ujItalok[i] = italok[i];
                }
                return ujItalok;
            }
        }

        static string[] KetListatOsszefuttat(string[] elsoLista, string[] masodikLista)
        {
            string[] ujLista = new string[elsoLista.Length + masodikLista.Length];
            int i = 0;
            int j = 0;
            int db = 0;
            while (i < elsoLista.Length && j < masodikLista.Length)
            {
                if (StringKisebbE(elsoLista[i], masodikLista[j]))
                {
                    ujLista[db] = elsoLista[i];
                    i++;
                }
                else if (StringKisebbE(masodikLista[j], elsoLista[i]))
                {
                    ujLista[db] = masodikLista[j];
                    j++;
                }
                else
                {
                    ujLista[db] = elsoLista[i];
                    i++;
                    j++;
                }
                db++;
            }
            while (i < elsoLista.Length)
            {
                ujLista[db] = elsoLista[i];
                i++;
                db++;
            }
            while (j < masodikLista.Length)
            {
                ujLista[db] = masodikLista[j];
                j++;
                db++;
            }
            string[] kimenet = new string[db];
            for (int k = 0; k < db; k++)
            {
                kimenet[k] = ujLista[k];
            }
            return kimenet;
        }

        static string[] Italok(string[,] italokNaponta)
        {
            // első napot kiválogatjuk, rendezzük, kiszedjük az ismétlődéseket
            // majd minden további napot kiválogatjuk, rendezzük, kiszedjük az ismétlődéseket ÉS összefuttatjuk az eddigi ital listával

            string[] italLista = AdottNapotKivalogat(italokNaponta, 0);
            NapiItallistaRendez(italLista);
            italLista = IsmetlodesekKiszurese(italLista);
            for (int i = 1; i < italokNaponta.GetLength(0); i++)
            {
                string[] aktualisNapiItalok = AdottNapotKivalogat(italokNaponta, i);
                NapiItallistaRendez(aktualisNapiItalok);
                aktualisNapiItalok = IsmetlodesekKiszurese(aktualisNapiItalok);
                italLista = KetListatOsszefuttat(italLista, aktualisNapiItalok);
            }
            return italLista;
        }

        static int[] EladottMennyisegek(string[,] italokNaponta, string[] italok)
        {
            int[] mennyisegek = new int[italok.Length];
            for (int italIndex = 0; italIndex < italok.Length; italIndex++)
            {
                for (int napIndex = 0; napIndex < italokNaponta.GetLength(0); napIndex++)
                {
                    int j = 0;
                    while (j < italokNaponta.GetLength(1) && italokNaponta[napIndex, j] != null)
                    {
                        if (italokNaponta[napIndex, j] == italok[italIndex])
                            mennyisegek[italIndex]++;
                        j++;
                    }
                }
            }
            return mennyisegek;
        }
    }
}

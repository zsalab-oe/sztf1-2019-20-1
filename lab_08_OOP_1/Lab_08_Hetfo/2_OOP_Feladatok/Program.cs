﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_OOP_Feladatok
{
    class Program
    {
        static void Main(string[] args)
        {
            Gyumolcs[] gyumik = new Gyumolcs[]
                {
                    new Gyumolcs("banán", 299, true, new string[] {"a", "c", "e", "k"}),
                    new Gyumolcs("szilva", 499, true, new string[] {"c", "b6", "e"}),
                    new Gyumolcs("alma", 1999, true, new string[] {"a", "b", "c", "d", "e", "f"}),
                    new Gyumolcs("narancs", 450, false, new string[] { "b6", "c", "k"})
                };


            int minIndex = LegolcsobbGyumolcs(gyumik);
            Console.WriteLine("Legolcsóbb: " + gyumik[minIndex].nev + ", egységára: " + gyumik[minIndex].egysegar);

            Gyumolcs legolcsobb = gyumik[minIndex];
            Console.WriteLine($"Legolcsóbb gyümölcs: {legolcsobb.nev}, egységára: {legolcsobb.egysegar}");

            Console.ReadLine();
        }

        static int LegolcsobbGyumolcs(Gyumolcs[] gyumik)
        {
            int minIndex = 0;
            for (int i = 1; i < gyumik.Length; i++)
            {
                if (gyumik[i].egysegar < gyumik[minIndex].egysegar)
                    minIndex = i;
            }
            return minIndex;
        }
    }
}

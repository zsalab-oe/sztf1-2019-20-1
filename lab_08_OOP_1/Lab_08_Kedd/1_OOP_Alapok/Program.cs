﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_OOP_Alapok
{
    // **** OOP bevezető ****

    // OSZTÁLY
    // - Az osztályok felfogható az objektumok sablonjaként (másképp: tekinthetünk rájuk úgy, mint egy süteményreceptre)
    // - meghatározza, hogy az osztályhoz tartozó objektumok hogyan jönnek létre, hogyan semmisülnek meg, milyen adatokat tartalmaznak, és ezek az adatok milyen módon módosíthatók

    // OBJEKTUM
    // - egy osztály egy példánya
    // - az osztály által meghatározott adatokat tartalmazza

    // Osztályok deklarálása:
    // class OsztályNeve { }
    // az osztály nevét követő kódblokkon belül határozzuk meg az osztály tagjait (egyelőre):
    // - adattagok
    // - konstruktorok
    // - metódusok


    class Sutemeny
    {
        //  **** LÁTHATÓSÁG ****

        // - a láthatóság határozza meg, hogy az osztály egyes tagjaira milyen széles körben hivatkozhatunk
        // PRIVATE: a privát láthatóság a legszigorúbb, ezzel a jelzővel ellátott tagok csak az osztályon belül hivatkozhatóak (tehát ebben az esetben a class Sutemeny { } kódblokkjában), vagyis egy másik osztályból nem férünk hozzá ezekhez az adatokhoz
        // PUBLIC: publikus láthatóság, vagyis nincs megkötés arra vonatkozóan, honnan férhetünk hozzá az adathoz
        // vannak egyéb láthatósági jelzők is, ezekkel későbbi félévekben foglalkozunk
        // - amennyiben mi magunk nem adunk meg láthatóságot módosító kulcsszót, úgy az alapértelmezett láthatósági szint kerül használatba
        // - osztály tagjainak alapértelmezett láthatósága: PRIVATE



        // **** ADATTAGOK ****

        // - változók, amelyeket osztályok esetén adattagoknak nevezünk
        // - ezekkel határozzuk meg, hogy milyen információt szeretnénk tárolni az osztályunk egy példányában
        // - minden egyes sütemény önálló értékekkel fog rendelkezni az alábbiakból

        private string nev;
        int lisztGramm; // alapértelmezett láthatósági szint: private
        public int tojasDb;
        public bool cukormaz;



        // **** KONSTRUKTOROK ****

        // - a konstruktor egy speciális metódus, amelynek nincs visszatérési értéke, és neve minden esetben megegyezik az osztály nevével
        // - az objektum(ok) létrehozásáért felel
        // - minden példányosításnál automatikusan meghívódik (példányosítás: adott osztályból készül egy új példány / objektum)
        // - egy osztály több konstruktort is tartalmazhat, más-más paraméretekkel (metódus túlterhelés elven)
        // - ha nem hozunk létre konstruktort, akkor és csak akkor a fordító automatikusan létrehoz egy paraméter nélküli alapértelmezett konstruktort

        public Sutemeny(string nev_, int lisztGramm, int tojasDb, bool cukormaz)
        {
            nev = nev_;
            this.lisztGramm = lisztGramm;
            this.tojasDb = tojasDb;
            this.cukormaz = cukormaz;
        }

        public Sutemeny(string nev)
        {
            this.nev = nev;
            Random rnd = new Random();
            this.lisztGramm = rnd.Next(0, 2000);
            tojasDb = rnd.Next(0, 100);
            cukormaz = true;
        }



        // **** METÓDUSOK ****

        public string Adatok()
        {
            string adatok = $"{nev}, hozzávalók: {lisztGramm} g liszt, {tojasDb} tojás, cukormáz: {cukormaz}";
            return adatok;
        }

        public void LisztHozzaadasa(int gramm)
        {
            lisztGramm += gramm;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            // **** PÉLDÁNYOSÍTÁS ****
            // new OsztályNeve(paraméterek)
            // - automatikus konstruktorhívás, az osztály valamely konstruktorának megfelelő paraméterezéssel

            // egy létrehozott objektumra egy, az objektum osztálya típusú változóval hivatkozhatunk
            // példában: a piskota változó Sutemeny típusú lesz, amely egy referencia típus => a piskota változó egy memóriacímet tárol (azt a memóriacímet, ahol a tényleges objektum elhelyezkedik a memóriában)

            Sutemeny piskota = new Sutemeny("piskóta", 120, 8, false);
            Sutemeny sosPerec = new Sutemeny("sós perec", 1000, 3, false);
            Sutemeny randomSuti = new Sutemeny("random süti");
            Sutemeny linzer = new Sutemeny("linzer", 500, 8, false);


            // osztályok és objektumok tagjainak elérése a "." operátorral lehetséges
            // változóNeve.tagNeve

            linzer.tojasDb = 20;
            Console.WriteLine(linzer.tojasDb);

            Console.WriteLine(randomSuti.Adatok());
            Console.WriteLine(piskota.Adatok());
            Console.WriteLine(sosPerec.Adatok());

            sosPerec.LisztHozzaadasa(200);

            Console.WriteLine(sosPerec.Adatok());

            Console.ReadLine();
        }
    }
}

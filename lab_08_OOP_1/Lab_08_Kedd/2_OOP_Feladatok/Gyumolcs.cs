﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_OOP_Feladatok
{
    class Gyumolcs
    {
        public string nev;
        public int egysegar;
        public bool finom;
        public string[] vitaminok;

        public Gyumolcs(string nev, int egysegar, bool finom, string vitaminokFelsorolas)
        {
            this.nev = nev;
            this.egysegar = egysegar;
            this.finom = finom;
            this.vitaminok = vitaminokFelsorolas.Split('*');
        }

        public string Adatok()
        {
            return $"{nev}, egységár: {egysegar}";
        }
    }
}

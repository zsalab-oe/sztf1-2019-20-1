﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_OOP_Feladatok
{
    class Program
    {
        static void Main(string[] args)
        {
            Gyumolcs[] gyumik = new Gyumolcs[]
            {
                new Gyumolcs("banán", 299, true, "a*b*c*e"),
                new Gyumolcs("narancs", 599, true, "c*d*a"),
                new Gyumolcs("szilva", 399, false, "c*a*b6"),
                new Gyumolcs("gránátalma", 1999, true, "a*b*c*e*f*d*x"),
                new Gyumolcs("citrom", 349, false, "c")
            };

            int minIndex = LegolcsobbGyumolcs(gyumik);
            Gyumolcs legolcsobb = null;
            legolcsobb = gyumik[minIndex];
            Console.WriteLine(legolcsobb.Adatok());
            // ugyanez
            Console.WriteLine(gyumik[minIndex].Adatok());

            Console.ReadLine();
        }

        static int LegolcsobbGyumolcs(Gyumolcs[] gyumik)
        {
            int minIndex = 0;
            for (int i = 1; i < gyumik.Length; i++)
            {
                if (gyumik[i].egysegar < gyumik[minIndex].egysegar)
                    minIndex = i;
            }
            return minIndex;
        }

        static int FinomGyumikSzama(Gyumolcs[] gyumik)
        {
            int db = 0;
            for (int i = 0; i < gyumik.Length; i++)
            {
                if (gyumik[i].finom)
                    db++;
            }
            return db;
        }

        static Gyumolcs[] FinomGyumik(Gyumolcs[] gyumik)
        {
            Gyumolcs[] finomGyumik = new Gyumolcs[FinomGyumikSzama(gyumik)];
            int db = 0;
            int j = 0;
            while (db < finomGyumik.Length)
            {
                if (gyumik[j].finom)
                {
                    finomGyumik[db] = gyumik[j];
                    db++;
                }
                j++;
            }
            return finomGyumik;
        }
    }
}

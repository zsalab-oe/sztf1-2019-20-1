﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Minta_ZH_GB
{
    class Program
    {
        static void Main(string[] args)
        {
            string adatsor = "Ausztrália;8;11;10#Dél-Korea;9;3;9#Egyesült Államok;46;37;38#Franciaország;10;18;14#Japán;12;8;21#Kína;26;18;26#Nagy-Britannia;27;23;17#Németország;17;10;15#Olaszország;8;12;8#Oroszország;19;17;20";
            string europaiOrszagokFelsorolas = "Albánia;Andorra;Ausztria;Azerbajdzsán;Belgium;BoszniaHercegovina;Bulgária;Ciprus;Csehország;Dánia;Nagy-Britannia;Észtország;Fehéroroszország;Finnország;Franciaország;Görögország;Grúzia;Hollandia;Horvátország;Írország;Izland;Kazahsztán;Lengyelország;Lettország;Liechtenstein;Litvánia;Luxemburg;Macedónia;Magyarország;Málta;Moldova;Monaco;Montenegró;Németország;Norvégia;Olaszország;Oroszország;Örményország;Portugália;Románia;SanMarino;Spanyolország;Svájc;Svédország;Szerbia;Szlovákia;Szlovénia;Törökország;Ukrajna;Vatikán";

            string[] europaiOrszagok = null;

            string[] orszagok = null;
            int[,] ermek = null;
            Feltolt(adatsor, ref orszagok, ref ermek);


            bool van = ValamilyenEremNelkul(ermek);

            Console.ReadLine();
        }

        static void TombKiir(string[] tomb)
        {
            for (int i = 0; i < tomb.Length; i++)
                Console.WriteLine(tomb[i]);
            Console.WriteLine();
        }

        static void Feltolt(string adatsor, ref string[] orszagok, ref int[,] ermek)
        {
            string[] orszagokAdatai = adatsor.Split('#');
            orszagok = new string[orszagokAdatai.Length];
            ermek = new int[orszagokAdatai.Length, 3];
            for (int i = 0; i < orszagok.Length; i++)
            {
                string[] egyOrszagAdatai = orszagokAdatai[i].Split(';');
                orszagok[i] = egyOrszagAdatai[0];
                for (int j = 0; j < ermek.GetLength(1); j++)
                {
                    ermek[i, j] = int.Parse(egyOrszagAdatai[j + 1]);
                }
            }

        }

        static bool ValamilyenEremNelkul(int[,] ermek)
        {
            int i = 0;
            bool talalt = false;
            while (i < ermek.GetLength(0) && !talalt)
            {
                int j = 0;
                while (j < ermek.GetLength(1) && ermek[i, j] != 0)
                {
                    j++;
                }
                if (j < ermek.GetLength(1))
                    talalt = true;
                i++;
            }
            return talalt;
        }

        static int[] EremKereses(int[,] ermek, int keresettErtek = 3)
        {
            int i = 0;
            int j = 0;
            bool talalt = false;
            while (i < ermek.GetLength(0) && !talalt)
            {
                j = 0;
                while (j < ermek.GetLength(1) && ermek[i, j] != keresettErtek)
                    j++;
                if (j < ermek.GetLength(1))
                    talalt = true;
                else
                    i++;
            }
            int[] vissza;
            if (talalt)
            {
                vissza = new int[] { i, j };
                return vissza;
                //return int[] { i, j };
            }
            else
            {
                vissza = new int[] { -1, -1 };
                return vissza;
                //return int[] { -1, -1 };
            }
        }

        static int LegkevesebbEzust(int[,] ermek)
        {
            int minIndex = 0;
            int minErtek = ermek[0, 1];
            for (int i = 1; i < ermek.GetLength(0); i++)
            {
                if (ermek[i, 1] < ermek[minIndex, 1])
                {
                    minIndex = i;
                    minErtek = ermek[minIndex, 1];
                }
            }
            return minIndex;
        }

        static int OrszagOsszErem(int[,] ermek, int orszagIndex)
        {
            int osszeg = 0;
            for (int j = 0; j < ermek.GetLength(1); j++)
            {
                osszeg += ermek[orszagIndex, j];
            }
            return osszeg;
        }

        static int[] ErmekSzamaOrszagonkent(int[,] ermek)
        {
            int[] ermekOrszagonkent = new int[ermek.GetLength(0)];
            for (int i = 0; i < ermek.GetLength(0); i++)
            {
                ermekOrszagonkent[i] = OrszagOsszErem(ermek, i);
            }
            return ermekOrszagonkent;
        }

        static int LegalabbTizArany(int[,] ermek)
        {
            int db = 0;
            for (int i = 0; i < ermek.GetLength(0); i++)
            {
                if (ermek[i, 0] >= 10)
                    db++;
            }
            return db;
        }

        static string[] LegalabbTizAranyatSzerzok(string[] orszagok, int[,] ermek)
        {
            int db = LegalabbTizArany(ermek);
            string[] legalabbTizArany = new string[db];
            db = 0;
            int i = 0;
            while (db < legalabbTizArany.Length)
            {
                if (ermek[i, 0] >= 10)
                {
                    legalabbTizArany[db] = orszagok[i];
                    db++;
                }
                i++;
            }
            return legalabbTizArany;
        }

        static string[] RendezesErmekSzerint(string[] orszagok, int[] ermekOrszagonkent)
        {
            return null;
        }

        static int MagyarHelyezes(string[] orszagokRendezve)
        {
            return -1;
        }

        static string[] LegjobbEuropaiOrszagok(string[] orszagok, string[] europaiOrszagok)
        {
            return null;
        }
    }
}

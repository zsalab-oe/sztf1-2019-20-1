﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Gyakorlas
{
    class Program
    {
        static void Main(string[] args)
        {
            // a deklaráció, a tömblétrehozás és az elemek megadása összevonható (ilyenkor a tömb elemszámát nem kell jeleznünk, kiderül az a megadott elemek darabszámából)

            int[] tomb = new int[] { 2, 34, 12, 4 };
            int[,] tomb2D = new int[,]
            {
                { 4, 1, 5},
                {3, 0 ,54 },
                {4 ,6 , 3 }
            };

            int[,] tomb2Dmasik = new int[,] { { 4, 1, 5}, {3, 0 ,54 }, {4 ,6 , 3 } };



            // ============================================================================================
            // Különbség az egyszerű maximumkiválasztás és az összetett feltételű maximumkiválasztás között

            // egyszerű: keressük a legnagyobb elem helyét
            int legnagyobbIndexe = LegnagyobbElemIndexe(tomb);
            Console.WriteLine("A legnagyobb elem indexe: " + legnagyobbIndexe);
            Console.WriteLine("A legnagyobb elem értéke: " + tomb[legnagyobbIndexe]);

            // összetett feltételű: csak az 5-tel osztható elemek közül keressük a legnagyobb elem helyét
            int legnagyobbOttelOszthatoIndexe = LegnagyobbOttelOszthatoElemIndexe(tomb);
            Console.WriteLine("A legnagyobb 5-tel osztható elem indexe (-1, ha nem volt 5-tel osztható elem): " + legnagyobbOttelOszthatoIndexe);
            if (legnagyobbOttelOszthatoIndexe >= 0)
                Console.WriteLine("A legnagyobb 5-tel osztható elem értéke: " + tomb[legnagyobbOttelOszthatoIndexe]);

            Console.ReadLine();
        }

        static int LegnagyobbElemIndexe(int[] tomb)
        {
            // kijelölöm a tömb első elemét, mint eddig talált legnagyobb elem
            // majd keresünk egy még ennél is nagyobb elemet, de ehhez elég csak az első elem UTÁNI elemeket vizsgálni, ezért indul a ciklus az 1-es indextől
            int maxIndex = 0;
            int maxErtek = tomb[0];
            for (int i = 1; i < tomb.Length; i++)
            {
                if (tomb[i] > maxErtek)
                {
                    maxIndex = i;
                    maxErtek = tomb[i];
                }
            }
            return maxIndex;
        }

        static int LegnagyobbOttelOszthatoElemIndexe(int[] tomb)
        {
            // most nem jelölhetem ki a tömb első elemét, mint eddig talált legnagyobb elem, mert nem lehetünk benne biztosak, hogy megfelel a másik feltételünknek is, esetünkben, hogy 5-tel osztható (sőt abban sem lehetünk biztosak, hogy van egyáltalán 5-tel osztható elem a tömbben), ezért egyelőre egy nem érvényes index értéket tárolunk a maxIndex változóban
            int maxIndex = -1;
            int maxErtek = int.MinValue; // int típusban eltárolható legkisebb érték
            for (int i = 0; i < tomb.Length; i++)
            {
                if (tomb[i] % 5 == 0 && tomb[i] > maxErtek)
                {
                    maxIndex = i;
                    maxErtek = tomb[i];
                }
            }
            return maxIndex;
        }

        static int LegnagyobbOttelOszthatoElemIndexeMasikMegoldas(int[] tomb)
        {
            // alternatív megoldás: az összetett feltételű maximumkiválasztás is megvalósítható úgy, hogy a maximum értéket nem tároljuk el egy maxErtek változóban
            // a feltételvizsgálathoz hozzáadunk még egy feltételt, cserébe megszabadulunk jó néhány (lassú) értékadástól :)

            int maxIndex = -1;
            for (int i = 0; i < tomb.Length; i++)
            {
                if (tomb[i] % 5 == 0 && (maxIndex == -1 || tomb[i] > tomb[maxIndex]))
                {
                    maxIndex = i;
                }
            }
            return maxIndex;
        }
    }
}

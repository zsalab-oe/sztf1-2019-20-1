﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_01_Hetfo
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int x;                  // 1. deklarálás
            x = 5;                  // 2. értékadás
            Console.WriteLine(x);   // 3. érték lekérdezése
            char karakter = 'A';    // deklarálás + értékadás
            Console.WriteLine(karakter);
            string targyNeve = "Szoftvertervezés és -fejlesztés I.";
            Console.WriteLine(targyNeve);
            bool ervenyes = true;
            Console.WriteLine(ervenyes);



            /* ======== Kasztolás ======== */

            // Kasztolni kell: ha értékvesztés TÖRTÉNHET
            //  - kisebb értelmezési tartományba történő konvertálásnál („történhet”, vagyis mindegy, hogy az aktuális érték pont beleférne – a típus a lényeg)
            //  - lebegőpontosból egésszé történő konvertálásnál
            //  - előjelesség szerinti típusváltásnál

            // Típuskényszerítés, "kasztolás" (ebben a félévben számok között):
            // célváltozó = (céltípus)forrásváltozó;

            int szam = -5;
            long szam2 = szam;
            byte szam3 = (byte)szam;
            //float pi = (float)3.14;
            double masikPi = 3.14;
            // tizedesponttal megadott literálok (értékek) alapértelmezetten double típusúak
            // számliterál megadása mindig tizedesPONTTAL!
            // string -> lebegőpontos konverzió esetén a windows lokalizációból szedi, hogy hogyan kell szövegesen tárolni a tizedes-elválasztót, ezt keresi a stringben (alapértelmezetten VESSZŐ)

            szam = (int)masikPi;
            Console.WriteLine(szam);
            string szoveg = szam.ToString();
            Console.WriteLine(szoveg);
            string szoveg2 = "123,456";
            double szam4 = double.Parse(szoveg2);
            Console.WriteLine(szam4);


            /* konverzió: bármilyen típus -> string */

            // Stringgé konvertálásnál:
            //célváltozó = forrásváltozó.ToString();
            string s = szam4.ToString();


            /* konverzió: string -> szám */

            // Stringből konvertálásnál:célváltozó = céltípus.Parse(stringváltozó);
            szam4 = double.Parse("10");

            Console.WriteLine("\n=============\n");

            int a = 4;
            int b = 3;
            int c = a + b;
            Console.WriteLine(c);
            int d = c / b; // 7 / 3
            Console.WriteLine(d);
            double e = c / (double)b;
            Console.WriteLine(e);
            double f = 7 / 3.0;
            Console.WriteLine(f);

            Console.WriteLine("\n====== ++ =======\n");

            x = 5;
            int y = x++;
            Console.WriteLine(x);
            Console.WriteLine(y);

            y = ++x;
            Console.WriteLine(x);
            Console.WriteLine(y);

            x = 5;
            y = ++x + 2;
            Console.WriteLine(x);
            Console.WriteLine(y);

            x = 5;
            y = x++ + ++x + x++;
            //   5  +  7  +  7
            Console.WriteLine(x);   // x = 8
            Console.WriteLine(y);   // y = 19



            Console.ReadLine();
        }
    }
}

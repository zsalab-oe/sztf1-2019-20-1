﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_01_Kedd
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");

            int szam;                   // 1. deklarálás
            szam = 5;                   // 2. értékadás
            Console.WriteLine(szam);    //3. érték lekérdezése
            int szam2 = 5;              // deklarálás + értékadás
            bool ervenyes = true;
            Console.WriteLine(ervenyes);
            const double pi = 3.14;
            // tizedesponttal megadott literálok (értékek) alapértelmezetten double típusúak
            // számliterál megadása mindig tizedesPONTTAL!
            // string -> lebegőpontos konverzió esetén a windows lokalizációból szedi, hogy hogyan kell szövegesen tárolni a tizedes-elválasztót, ezt keresi a stringben (alapértelmezetten VESSZŐ)

            Console.WriteLine(pi);
            char karakter = 'A';
            Console.WriteLine(karakter);
            string szoveg = "bármilyen szöveg";
            Console.WriteLine(szoveg);



            /* ======== Kasztolás ======== */

            // Kasztolni kell: ha értékvesztés TÖRTÉNHET
            //  - kisebb értelmezési tartományba történő konvertálásnál („történhet”, vagyis mindegy, hogy az aktuális érték pont beleférne – a típus a lényeg)
            //  - lebegőpontosból egésszé történő konvertálásnál
            //  - előjelesség szerinti típusváltásnál

            // Típuskényszerítés, "kasztolás" (ebben a félévben számok között):
            // célváltozó = (céltípus)forrásváltozó;

            int a = -5;
            long b = a;
            double c = a;
            byte d = (byte)a;
            float e = (float)c;
            decimal f = (decimal)e;

            double g = 1.234;
            decimal h = (decimal)1.234;
            decimal i = 1.234m;
            float j = 1.234f;
            float k = (float)h;

            int l = 4;
            uint m = (uint)l;


            /* konverzió: bármilyen típus -> string */

            // Stringgé konvertálásnál:
            //célváltozó = forrásváltozó.ToString();
            string s = a.ToString();


            /* konverzió: string -> szám */

            // Stringből konvertálásnál:célváltozó = céltípus.Parse(stringváltozó);
            a = int.Parse("10");

            //=========================

            Console.WriteLine("\n============\n");

            int x = 2;
            int y = x + 3;
            int maradek = y % x;
            Console.WriteLine(maradek);
            x += 6; // x = x + 6
            Console.WriteLine(x);
            y = x << 1;
            Console.WriteLine(y);
            y <<= 2;
            Console.WriteLine(y);

            Console.WriteLine("\n====== ++ ======\n");

            x = 2;
            x++;
            Console.WriteLine(x);
            //x = 3
            y = x++;
            Console.WriteLine(x);
            Console.WriteLine(y);

            x = 5;
            y = ++x;
            Console.WriteLine(x);
            Console.WriteLine(y);

            x = 5;
            y = x++ + ++x + x++;
            //   5  +  7  +  7
            Console.WriteLine(x);   // x = 8
            Console.WriteLine(y);   // y = 19

            Console.WriteLine("Írj be egy szöveget!");
            string beolvasottSzoveg = Console.ReadLine();

            Console.Write("A beírt szöveg: ");
            Console.WriteLine(beolvasottSzoveg);

            Console.WriteLine("\n============\n");

            Console.WriteLine("Írj be egy számot!");
            beolvasottSzoveg = Console.ReadLine();
            int beolvasottSzam = int.Parse(beolvasottSzoveg);


            Console.Write("A beírt szám: ");
            Console.WriteLine(beolvasottSzam);


            Console.ReadLine();
        }
    }
}

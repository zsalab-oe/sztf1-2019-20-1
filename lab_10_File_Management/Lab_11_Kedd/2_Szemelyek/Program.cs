﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace _2_Szemelyek
{
    class Program
    {
        static void Main(string[] args)
        {
            string mappa = Directory.GetCurrentDirectory();
            SzemelyKezelo kezelo = new SzemelyKezelo(mappa);

            bool kilepes = false;
            do
            {
                Menu(kezelo);

                Console.Write("Kilép? (y/n) ");
                char c = char.ToLower(Console.ReadKey().KeyChar);
                if (c == 'y')
                    kilepes = true;
            } while (!kilepes);

            Console.ReadLine();
        }

        static void Menu(SzemelyKezelo kezelo)
        {
            Console.Clear();
            Console.WriteLine("1: Listázás");
            Console.WriteLine("2: Felvétel");
            Console.WriteLine("3: Törlés");
            int valasztott = int.Parse(Console.ReadLine());
            if (valasztott == 1)
            {
                Szemely[] szemelies = kezelo.Listaz();
                for (int i = 0; i < szemelies.Length; i++)
                {
                    string vNev = szemelies[i].Adatok[0];
                    string kNev = szemelies[i].Adatok[1];
                    Console.WriteLine(vNev + " " + kNev);
                }
            }
            else if (valasztott == 2)
            {
                Console.Write("Vezetéknév: ");
                string vezeteknev = Console.ReadLine();
                Console.Write("Keresztnév: ");
                string keresztnev = Console.ReadLine();
                Console.Write("Életkor: ");
                int eletkor = int.Parse(Console.ReadLine());
                Console.Write("Foglalkozás: ");
                string foglalkozas = Console.ReadLine();
                Szemely uj = new Szemely(vezeteknev, keresztnev, eletkor, foglalkozas);
                kezelo.Hozzaad(uj);
            }
            else if (valasztott == 3)
            {
                Szemely[] szemelyek = kezelo.Listaz();
                for (int i = 0; i < szemelyek.Length; i++)
                {
                    string vezeteknev = szemelyek[i].Adatok[0];
                    string keresztnev = szemelyek[i].Adatok[1];
                    Console.WriteLine($"[{i}] {vezeteknev} {keresztnev}");
                }
                Console.WriteLine("Melyiket szeretnéd törölni?");
                int torlendo = int.Parse(Console.ReadLine());
                kezelo.Torol(szemelyek[torlendo]);
            }
        }
    }
}

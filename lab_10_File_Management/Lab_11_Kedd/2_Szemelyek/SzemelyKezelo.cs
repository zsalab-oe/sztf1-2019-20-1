﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace _2_Szemelyek
{
    class SzemelyKezelo
    {
        string mappa;

        public SzemelyKezelo(string mappa)
        {
            this.mappa = mappa;
        }

        private string Fajlnev(Szemely sz)
        {
            string[] adatok = sz.Adatok;
            string vNev = adatok[0];
            string kNev = adatok[1];
            string eletkorS = adatok[2];
            string fajlnev = $"{vNev[0]}_{kNev[0]}_{eletkorS}.dat";
            return fajlnev;
        }

        public void Hozzaad(Szemely uj)
        {
            string eleresiUt = mappa + "\\" + Fajlnev(uj);
            File.WriteAllLines(eleresiUt, uj.Adatok, Encoding.Default);
        }

        public Szemely[] Listaz()
        {
            string[] fajlok = Directory.GetFiles(mappa, "*.dat"); // összegyűjti a keresésnek megfelelő fájlok elérési útjait egy string tömbbe
            Szemely[] szemelyek = new Szemely[fajlok.Length];
            for (int i = 0; i < szemelyek.Length; i++)
            {
                string[] szemelyAdatai = File.ReadAllLines(fajlok[i]);
                string vNev = szemelyAdatai[0];
                string kNev = szemelyAdatai[1];
                int eletkor = int.Parse(szemelyAdatai[2]);
                string foglalkozas = szemelyAdatai[3];

                szemelyek[i] = new Szemely(vNev, kNev, eletkor, foglalkozas);
            }
            return szemelyek;
        }

        public void Torol(Szemely torlendo)
        {
            string eleresiUt = mappa + "\\" + Fajlnev(torlendo);
            File.Delete(eleresiUt);
        }
    }
}

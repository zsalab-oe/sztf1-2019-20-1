﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Szemelyek
{
    class Szemely
    {
        private string vNev;
        private string kNev;
        private int eletkor;
        private string foglalkozas;

        public string[] Adatok
        {
            get
            {
                string[] adatok = new string[] {vNev, kNev, eletkor.ToString(), foglalkozas };
                return adatok;
                //return new string[] { vNev, kNev, eletkor.ToString(), foglalkozas };
            }
        }

        public Szemely(string vNev, string kNev, int eletkor, string foglalkozas)
        {
            this.vNev = vNev;
            this.kNev = kNev;
            this.eletkor = eletkor;
            this.foglalkozas = foglalkozas;
        }
    }
}

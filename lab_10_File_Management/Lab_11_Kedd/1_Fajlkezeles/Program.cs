﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO; // <-- jelzés, hogy "használjuk" a névteret

namespace _1_Fajlkezeles
{
    class Program
    {
        static void Main(string[] args)
        {
            // a StreamReader, StreamWriter, File és egyéb fájlkezeléssel kapcsolatos osztályok a System.IO névtérben vannak
            // ha nem szeretnénk, hogy ezen osztályoknál minden hivatkozást névtérrel együtt kelljen megadni, akkor az adott .cs fájl elején jelezhetjük, hogy használjuk a névteret a "using System.IO;" kulcsszavakkal
            // onnantól kezdve a fordító tudja, hogy pl. a StreamWriter hivatkozásnál valójában a System.IO.StreamWriter osztályra hivatkozunk


            string eleresiUt = "fajl.txt";
            string szoveg = "valami";
            Iras(eleresiUt, szoveg);

            string[] szovegek = new string[] { "első sor", "második sor", "és még egy sor" };
            Iras(eleresiUt, szovegek);

            Beolvas(eleresiUt);

            File.AppendAllLines(eleresiUt, szovegek, Encoding.Default);

            string teljes = File.ReadAllText(eleresiUt, Encoding.Default);
            Console.WriteLine("=========");
            Console.WriteLine(teljes);


            Console.ReadLine();
        }

        static void Iras(string eleresiUt, string szoveg)
        {
            // ha "használjuk" a névteret, az osztályra való hivatkozás egyszerűsíthető, System.IO. elhagyható
            // ugyanakkor nem tilos névtérrel együtt hivatkozni az osztályra
            System.IO.StreamWriter sw = new System.IO.StreamWriter(eleresiUt, false, Encoding.Default);
            sw.WriteLine(szoveg);
            sw.Close();
        }

        static void Iras(string eleresiUt, string[] sTomb)
        {
            StreamWriter sw = new StreamWriter(eleresiUt, true, Encoding.Default);
            for (int i = 0; i < sTomb.Length; i++)
            {
                sw.WriteLine(sTomb[i]);
            }

            sw.Close();
        }

        static void Beolvas(string eleresiUt)
        {
            StreamReader sr = new StreamReader(eleresiUt, Encoding.Default);
            while (!sr.EndOfStream)
            {
                string sor = sr.ReadLine();
                Console.WriteLine(sor);
            }
            sr.Close();
        }
    }
}

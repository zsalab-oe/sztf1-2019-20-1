﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace _2_Szemelyek
{
    class SzemelyKezelo
    {
        private string mappa;

        public SzemelyKezelo(string mappa)
        {
            this.mappa = mappa;
        }

        private string FajlNev(Szemely sz)
        {
            string[] adatok = sz.Adatok;
            string vezeteknev = adatok[0];
            string keresztnev = adatok[1];
            int eletkor = int.Parse(adatok[2]);
            string fajlNev = $"{vezeteknev[0]}_{keresztnev[0]}_{eletkor.ToString()}.dat";
            return fajlNev;
        }

        public void Hozzaad(Szemely uj)
        {
            string eleresiUt = mappa + @"\" + FajlNev(uj);
            // System.IO.
            File.WriteAllLines(eleresiUt, uj.Adatok, Encoding.Default);
        }

        public Szemely[] Listaz()
        {
            string[] fajlok = Directory.GetFiles(this.mappa, "*.dat"); // összegyűjti a keresésnek megfelelő fájlok elérési útjait egy string tömbbe
            Szemely[] szemelyek = new Szemely[fajlok.Length];
            for (int i = 0; i < szemelyek.Length; i++)
            {
                string[] szemelyAdatai = File.ReadAllLines(fajlok[i]);
                string vezeteknev = szemelyAdatai[0];
                string keresztnev = szemelyAdatai[1];
                int eletkor = int.Parse(szemelyAdatai[2]);
                string foglalkozas = szemelyAdatai[1];
                szemelyek[i] = new Szemely(vezeteknev, keresztnev, eletkor, foglalkozas);
            }
            return szemelyek;
        }

        public void Torol(Szemely torlendo)
        {
            string eleresiUt = this.mappa + "\\" + FajlNev(torlendo);
            File.Delete(eleresiUt);
        }
    }
}

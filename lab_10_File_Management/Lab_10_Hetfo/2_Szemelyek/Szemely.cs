﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Szemelyek
{
    class Szemely
    {
        private string vezeteknev;
        private string keresztnev;
        private int eletkor;
        private string foglalkozas;

        public string[] Adatok
        {
            get { return new string[] { vezeteknev, keresztnev, eletkor.ToString(), foglalkozas }; }
        }


        // Ctrl + .
        public Szemely(string vezeteknev, string keresztnev, int eletkor, string foglalkozas)
        {
            this.vezeteknev = vezeteknev;
            this.keresztnev = keresztnev;
            this.eletkor = eletkor;
            this.foglalkozas = foglalkozas;
        }
    }
}

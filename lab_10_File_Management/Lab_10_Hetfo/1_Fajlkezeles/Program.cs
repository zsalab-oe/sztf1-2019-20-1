﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO; // <-- jelzés, hogy "használjuk" a névteret

namespace _1_Fajlkezeles
{
    class Program
    {
        static void Main(string[] args)
        {
            // a StreamReader, StreamWriter, File és egyéb fájlkezeléssel kapcsolatos osztályok a System.IO névtérben vannak
            // ha nem szeretnénk, hogy ezen osztályoknál minden hivatkozást névtérrel együtt kelljen megadni, akkor az adott .cs fájl elején jelezhetjük, hogy használjuk a névteret a "using System.IO;" kulcsszavakkal
            // onnantól kezdve a fordító tudja, hogy pl. a StreamWriter hivatkozásnál valójában a System.IO.StreamWriter osztályra hivatkozunk


            string eleresiUt = "fajl.txt";

            BeolvasSR(eleresiUt);
            string teljesSzoveg = File.ReadAllText(eleresiUt, Encoding.Default);
            Console.WriteLine();
            Console.WriteLine(teljesSzoveg);

            IrasSW(eleresiUt);
            File.WriteAllText(eleresiUt, "most meg ezzel írd felül", Encoding.Default);

            string[] sorok = File.ReadAllLines(eleresiUt, Encoding.Default);
            for (int i = 0; i < sorok.Length; i++)
            {
                sorok[i] += ", valami";
            }
            File.WriteAllLines(eleresiUt, sorok, Encoding.Default);

            Console.ReadLine();
        }

        static void BeolvasSR(string eleresiUt)
        {
            // ha "használjuk" a névteret, az osztályra való hivatkozás egyszerűsíthető, System.IO. elhagyható
            // ugyanakkor nem tilos névtérrel együtt hivatkozni az osztályra
            System.IO.StreamReader sr = new System.IO.StreamReader(eleresiUt, Encoding.Default);
            while (!sr.EndOfStream)
            {
                string sor = sr.ReadLine();
                Console.WriteLine(sor);
            }
            sr.Close();
        }

        static void IrasSW(string eleresiUt)
        {
            StreamWriter sw = new StreamWriter(eleresiUt, false, Encoding.Default);
            sw.WriteLine("ezzel írd felül");
            sw.Close();
        }
    }
}

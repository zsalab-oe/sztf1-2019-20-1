﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Stringmuveletek
{
    class Program
    {
        static void Main(string[] args)
        {
            int szam = 5;
            Console.WriteLine(5 + 1 + " egyenlő hat");
            Console.WriteLine("hat egyenlő " + 5 + 1);
            Console.WriteLine("hat egyenlő " + (5 + 1));

            string s1, s2;
            s1 = "Valamilyen szöveg.";
            s2 = s1.Substring(4, 6);
            Console.WriteLine(s2);

            bool benneVan = s1.Contains("a");
            Console.WriteLine(benneVan);
            benneVan = s1.Contains("szöveg");
            Console.WriteLine(benneVan);
            benneVan = s1.Contains("ez nincs benne");
            Console.WriteLine(benneVan);

            int index;
            index = s1.IndexOf('a');
            Console.WriteLine(index);
            index = s1.IndexOf("szöveg");
            Console.WriteLine(index);
            index = s1.IndexOf("ez nincs benne");
            Console.WriteLine(index);

            char c = s1.ElementAt(5); //s1[5]

            bool pontraVegzodik = s1.EndsWith('.'.ToString());

            bool egyenlo = s1.Equals(s2); // s1 == s2

            s1 = s1.PadLeft(25, '*');
            Console.WriteLine(s1);

            s2 = s1.Trim(new char[] { '*', '.' });
            Console.WriteLine(s2);

            s2 = s1.Replace("szöveg", "karaktersorozat");
            Console.WriteLine(s2);

            s2 = s1.ToUpper();
            Console.WriteLine(s2);



            Console.ReadLine();
        }
    }
}

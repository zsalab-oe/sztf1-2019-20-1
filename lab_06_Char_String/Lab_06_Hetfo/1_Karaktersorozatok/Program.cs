﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Karaktersorozatok
{
    class Program
    {
        static void Main(string[] args)
        {
            string s = "abc";
            s += 'd'; // s = s + 'd'
            s = 'd'.ToString();
            char c = s.ToCharArray()[0];
            c = s[0];

            Random rnd = new Random();
            string neptunkod = NeptunkodGeneralas(rnd, 6);
            Console.WriteLine(neptunkod);
            string szoveg = "Ez egy karaktersorozat.";
            Console.WriteLine(">> " + szoveg);
            Console.WriteLine("Betűk száma: " + BetukSzama(szoveg));
            Console.WriteLine("Magánhangzók száma: " + MaganhangzokSzama(szoveg));


            Console.WriteLine(Palindrom("Géza, kék az ég."));
            //gézakékazég

            Console.ReadLine();
        }

        static string NeptunkodGeneralas(Random rnd, int hossz)
        {
            string neptunkod = "";
            for (int i = 0; i < hossz; i++)
            {
                // számjegyek és betűk lehetnek benne 50-50% arányban
                if (rnd.Next(0,2) == 0)
                {
                    // számjegyet generálunk
                    neptunkod += rnd.Next(0, 10);
                }
                else
                {
                    // betűt generálunk
                    neptunkod += (char)rnd.Next('A', 'Z' + 1); // karakter generálás karakterkód alapján
                }
            }
            return neptunkod;
        }

        static int BetukSzama(string s)
        {
            int db = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsLetter(s[i]))
                    db++;
            }
            return db;
        }

        static bool Maganhangzo(char c)
        {
            string maganhangzok = "aáeéiíoóöőuúüű";
            int j = 0;
            c = char.ToLower(c);
            while (j < maganhangzok.Length && c != maganhangzok[j])
                j++;
            //if (j < maganhangzok.Length)
            //    return true;
            //else
            //    return false;
            return j < maganhangzok.Length;
        }

        static int MaganhangzokSzama(string s)
        {
            int db = 0;
            for (int i = 0; i < s.Length; i++)
            {
                if (Maganhangzo(s[i]))
                    db++;
            }
            return db;
        }

        static string Atalakit(string s)
        {
            string atalakitott = "";
            for (int i = 0; i < s.Length; i++)
            {
                atalakitott += s[i];
                if (Maganhangzo(s[i]))
                    atalakitott += 'v'.ToString() + s[i];
            }
            return atalakitott;
        }

        static string CsakBetukEsSzamok(string s)
        {
            string atalakitott = "";
            for (int i = 0; i < s.Length; i++)
            {
                if (char.IsLetterOrDigit(s[i]))
                    atalakitott += s[i];
            }
            return atalakitott;
        }

        static string Kisbetus(string s)
        {
            string kisbetus = "";
            for (int i = 0; i < s.Length; i++)
            {
                kisbetus += char.ToLower(s[i]);
            }
            return kisbetus;
        }

        static bool Palindrom(string s)
        {
            s = CsakBetukEsSzamok(s);
            s = Kisbetus(s);
            int j = 0;
            while (j < s.Length / 2 && s[j] == s[s.Length - 1 - j])
                j++;
            return j >= s.Length / 2;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab06_StringMethods
{
    class Program
    {
        static Random random = new Random();

        static string GenerateNeptunID(int length)
        {
            // Neptun kód generálása
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            string neptunID = "";
            // az első karakter mindig betű
            // karakter generálás egy karaktergyűjtemény alapján
            neptunID += alphabet[random.Next(alphabet.Length)];
            // "length" darab karaterből áll
            for (int i = 0; i < length - 1; i++)
            {
                // betűk és számok vannak benne 70-30% arányban
                if (random.Next(100) < 30)
                {
                    // számot generálunk
                    neptunID += random.Next(10);
                    // neptunID = neptunID + random.Next(10);
                }
                else
                {
                    // betűt generálunk
                    // karakter generálás karakterkód alapján
                    neptunID += (char)random.Next((int)'a', (int)'z' + 1);
                }
            }
            return neptunID;
        }

        static int HowManyLetters(string text)
        {
            int count = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (char.IsLetter(text[i]))
                {
                    count++;
                }
            }
            return count;
        }

        static bool IsVowel(char c)
        {
            string vowels = "aáeéiíoóöőuúüű";
            //int i = 0;
            //while (i < vowels.Length && vowels[i] != char.ToLower(c))
            //{
            //    i++;
            //}
            //return i < vowels.Length;
            return vowels.Contains(char.ToLower(c));
        }

        static int HowManyVowels(string text)
        {
            int count = 0;
            for (int i = 0; i < text.Length; i++)
            {
                if (IsVowel(text[i]))
                {
                    count++;
                }
            }
            return count;
        }

        static string ConvertToStrangeString(string text)
        {
            string converted = "";
            for (int i = 0; i < text.Length; i++)
            {
                char currentChar = text[i];
                if (IsVowel(currentChar))
                {
                    converted += currentChar + "v" + currentChar;
                }
                else
                {
                    converted += currentChar;
                }
            }
            return converted;
        }

        static string RemoveWhitespaces(string text)
        {
            //string converted = "";
            //for (int i = 0; i < text.Length; i++)
            //{
            //    if (text[i] == ' ' || text[i] == '\t')
            //    {
            //        // pass
            //    }
            //    else
            //    {
            //        converted += text[i];
            //    }
            //}
            //return converted;            
            return text.Replace(" ", "");
        }

        static bool IsPalindrome(string text)
        {
            string convertedText = RemoveWhitespaces(text);

            int i = 0;
            while (i < convertedText.Length / 2 && convertedText[i] == convertedText[convertedText.Length - 1 - i])
            {
                i++;
            }
            return i >= convertedText.Length / 2;
        }

        static void Main(string[] args)
        {
            //char c = 'a';
            //char[] text = new char[] { 'v', 'a', 'l', 'a', 'm', 'i' };
            //text[0] = 'V';

            //string text2 = "valami";    // immutable type
            //// text2[0] = 'V';
            //text2 = text2 + "valamimás";
            //Console.WriteLine(text2);

            //char symbolA = 'a';
            //char symbolB = '1';
            //char symbolC = '?';

            //Console.WriteLine(char.IsDigit(symbolA));
            //Console.WriteLine(char.IsDigit(symbolB));
            //Console.WriteLine(char.IsDigit(symbolC));

            //string neptunID;
            //do
            //{
            //    neptunID = GenerateNeptunID(6);
            //    Console.WriteLine(neptunID.ToUpper());
            //    Console.WriteLine(neptunID);
            //} while (neptunID != "fvb2d1");

            string testText = "Hét szép szűzlány őrült írót nyúz.";
            Console.WriteLine($"Ennyi darab magánhangzó van a szövegben: {HowManyVowels(testText)}");

            string testText2 = "TE tUdsz Így beszélni";
            Console.WriteLine(ConvertToStrangeString(testText2));
            string testText3 = "Ez egy BÁRMIT nagyon jó szöveg";
            int idx = testText3.IndexOf("nagyon");
            string testText3Removed = testText3.Remove(0, idx);
            Console.WriteLine(testText3Removed);

            // kék
            // indulagörögaludni    // palindrom szöveg
            Console.WriteLine("Palindrom szöveg-e?" + IsPalindrome("indulagörögaludni"));            
        }
    }
}

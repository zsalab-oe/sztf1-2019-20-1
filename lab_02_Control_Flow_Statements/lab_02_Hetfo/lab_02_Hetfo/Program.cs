﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab_02_Hetfo
{
    class Program
    {
        static void Main(string[] args)
        {
            //int x = 5;
            //x = 10;
            //const int y = 5; /* komment */ int z = 0;
            ///* 1. sor
            // 2. sor
            // asdfasdas             
            // */
            ////y = 10;




            /* elágazás */

            Console.WriteLine(">> Elágazás\n");

            Console.Write("Név: ");
            string nev = Console.ReadLine();
            if (nev == "Béla")
            {
                Console.WriteLine("SZIA");
            }
            else if (nev == "Józsi")
            {
                Console.WriteLine("Hi");
            }
            else
            {
                Console.WriteLine("HELLÓ");
            }

            Console.WriteLine("--------");

            Console.Write("Osztandó: ");
            int osztando = int.Parse(Console.ReadLine());
            Console.Write("Osztó: ");
            int oszto = int.Parse(Console.ReadLine());

            if (oszto != 0 && osztando % oszto == 0)
            {
                Console.WriteLine("Osztható");
                Console.WriteLine("Ezt is írjuk ki igaz feltétel esetén");
            }
            else
            {
                Console.WriteLine("Nem osztható");
            }


            Console.ReadLine();
            Console.Clear();



            /* hátul tesztelő ciklus */

            Console.WriteLine(">> Hátul tesztelő ciklus\n");

            string name = "";
            do
            {
                Console.Write("Név: ");
                name = Console.ReadLine();
            } while (name == "" || name == "Shakespeare");

            Console.WriteLine("Végre kaptam valamit! Szia, " + name + "!");


            Console.ReadLine();
            Console.Clear();



            /* elöl tesztelő ciklus */

            Console.WriteLine(">> Elöl tesztelő ciklus\n");

            int tol = 5;
            int ig = 10;
            int j = tol;
            while (j <= ig)
            {
                Console.WriteLine(j);
                j++;
            }

            Console.WriteLine("--------");

            int x = 1;
            Console.WriteLine(x);
            x = x << 1;
            Console.WriteLine(x);
            x <<= 1;
            Console.WriteLine(x);
            int i = 0;
            while (i <= 10)
            {
                Console.WriteLine(x);
                x <<= 1;
                i++;
            }


            Console.ReadLine();
            Console.Clear();



            /* Gyakorlófeladat - gondolt szám */

            Console.WriteLine(">> Gyakorlófeladat - gondolt szám\n");

            Random R = new Random();
            int gondoltSzam = R.Next(0, 101);
            int bekertSzam;
            int probalkozasokSzama = 0;
            do
            {
                Console.Write("Tipp: ");
                bekertSzam = int.Parse(Console.ReadLine());
                if (gondoltSzam < bekertSzam)
                    Console.WriteLine("A gondolt szám ennél kisebb.");
                else if (gondoltSzam > bekertSzam)
                    Console.WriteLine("A gondolt szám ennél nagyobb.");
                probalkozasokSzama++;
            } while (bekertSzam != gondoltSzam);

            Console.WriteLine("Gratulálok, megvan {0}. próbálkozásra!", probalkozasokSzama);


            Console.ReadLine();
            Console.Clear();



            /* hatványozás */
            
            Console.WriteLine(">> Gyakorlófeladat - hatványozás\n");

            Console.Write("a: ");
            int a = int.Parse(Console.ReadLine());
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("a^n = ");
            int eredm = 1;
            int k = 0;
            while (k < n)
            {
                eredm *= a;
                k++;
            }
            Console.WriteLine(eredm);


            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_02_Kedd
{
    class Program
    {
        static void Main(string[] args)
        {
            int szam1 = 5;
            szam1 = 10;
            const int szam2 = 5;

            int a = 10;
            int b = 4;
            int c = a / b;
            float d = a / b;
            float e = a / (float)b;
            float f = 10;
            float g = 4;
            int h = (int)(f / g);


            /* elágazás */

            Console.WriteLine(">> Elágazás\n");

            Console.Write("Név: ");
            string nev = Console.ReadLine();
            if (nev == "Béla")
                Console.WriteLine("Szia!");
            else if (nev == "Józsi")
                Console.WriteLine("Szevasz!");
            else
                Console.WriteLine("Helló!");

            Console.WriteLine("--------");

            Console.Write("x: ");
            int szam = int.Parse(Console.ReadLine());
            bool paros;
            if (szam % 2 == 0)
                paros = true;
            else
                paros = false;

            Console.WriteLine("Páros: " + paros);

            
            Console.ReadLine();
            Console.Clear();

            /* elöl tesztelő ciklus */

            Console.WriteLine(">> Elöl tesztelő ciklus\n");

            int x = 10;
            while (x < 10)
            {
                Console.WriteLine(x++);
                //x++;
            }

            Console.WriteLine("----------");

            /* hátul tesztelő ciklus */

            Console.WriteLine(">> Hátul tesztelő ciklus\n");

            x = 10;
            do
            {
                Console.WriteLine(x++);
            } while (x < 10);


            Console.ReadLine();
            Console.Clear();
            Console.WriteLine(">> Hátul tesztelő ciklus - Shakespeare\n");

            string bekertNev = "";
            do
            {
                Console.Write("Név: ");
                bekertNev = Console.ReadLine();
            } while (bekertNev == "" || bekertNev == "Shakespeare");
            Console.WriteLine("Végre kaptam valamit! Szia, {0}!", bekertNev);


            Console.ReadLine();
            Console.Clear();



            /* Gyakorlófeladat - gondolt szám */

            Console.WriteLine(">> Gyakorlófeladat - gondolt szám\n");

            Random R = new Random();
            int gondoltSzam = R.Next(0, 101);
            int bekertSzam;
            int probalkozasokSzama = 0;
            do
            {
                Console.Write("Tipp: ");
                bekertSzam = int.Parse(Console.ReadLine());
                if (gondoltSzam < bekertSzam)
                    Console.WriteLine("A gondolt szám ennél kisebb.");
                else if (gondoltSzam > bekertSzam)
                    Console.WriteLine("A gondolt szám ennél nagyobb.");
                probalkozasokSzama++;
            } while (bekertSzam != gondoltSzam);
            Console.WriteLine("Gratulálok! Megvan {0}. próbálkozásra", probalkozasokSzama);


            Console.ReadLine();
            Console.Clear();



            /* hatványozás */

            Console.WriteLine(">> Gyakorlófeladat - hatványozás, faktoriális\n");

            Console.Write("a: ");
            int alap = int.Parse(Console.ReadLine());
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());
            long eredm = 1;
            int j = 1;
            while (j <= n)
            {
                eredm *= alap;
                j++;
            }

            Console.WriteLine($"{alap}^{n} = {eredm}");

            /* faktoriális */

            eredm = 1;
            j = 2;
            while (j <= n)
            {
                eredm *= j;
                j++;
            }
            Console.WriteLine($"{n}! = {eredm}");



            Console.ReadLine();
        }
    }
}

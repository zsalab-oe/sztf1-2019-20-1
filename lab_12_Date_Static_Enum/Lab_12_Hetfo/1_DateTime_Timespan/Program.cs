﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_DateTime_Timespan
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime most;
            most = DateTime.Now;
            Console.WriteLine(most.ToString());
            Console.WriteLine(most.Day);
            Console.WriteLine(most.DayOfWeek);

            DateTime ezerNappalEzelott = most.AddDays(-1000);
            Console.WriteLine(ezerNappalEzelott.DayOfWeek);
            Console.WriteLine(ezerNappalEzelott);

            DateTime ezredfordulo = new DateTime(2000, 1, 1, 0, 0, 0);

            TimeSpan elteltIdo = most - ezredfordulo;
            Console.WriteLine("Eltelt idő: " + elteltIdo);
            Console.WriteLine("Eltelt napok:" + elteltIdo.Days);


            Console.ReadLine();
        }
    }
}

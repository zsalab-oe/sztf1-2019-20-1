﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Enum
{
    // saját felsorolástípus létrehozása:
    // enum FelsorolásTípusNeve { lehetségesÉrték1, lehetségesÉrték2, ..., lehetségesÉrtékN }
    enum Nap
    {
        Hetfo, Kedd, Szerda, Csutortok, Pentek, Szombat, Vasarnap
    }

    class Program
    {
        static void Main(string[] args)
        {
            Nap ma = Nap.Kedd;

            // konverzió: felsorolástípus -> egész szám
            int szam = (int)ma;
            Console.WriteLine(szam);

            // konverzió: egész szám -> felsorolástípus
            Nap holnap = (Nap)(szam + 1);
            Nap milyenNap = (Nap)4;

            // konverzió: felsorolástípus -> string
            string holnapSzovegesen = holnap.ToString();
            Console.WriteLine(holnapSzovegesen);

            // konverzió: string -> felsorolástípus
            Nap megbeszeles = (Nap)System.Enum.Parse(typeof(Nap), "Csutortok");

            Console.WriteLine("Megbeszélés: " + megbeszeles);

            megbeszeles++;

            Console.WriteLine("Megbeszélés új időpontja: " + megbeszeles);

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_DateTime_TimeSpan
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime most = DateTime.Now;
            Console.WriteLine(most.ToString());
            Console.WriteLine(most.DayOfWeek);

            DateTime ezerNappalEzelott = most.AddDays(-1000);
            Console.WriteLine(ezerNappalEzelott);
            Console.WriteLine(ezerNappalEzelott.DayOfWeek);

            DateTime ezredfordulo = new DateTime(2000, 1, 1, 0,0,0);

            TimeSpan elteltIdo = most - ezredfordulo;
            Console.WriteLine("Eltelt napok:" + elteltIdo.TotalDays);
            Console.WriteLine("Eltelt idő: " + elteltIdo.ToString());
        }
    }
}

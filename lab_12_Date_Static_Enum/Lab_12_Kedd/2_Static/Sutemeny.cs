﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Static
{
    class Sutemeny
    {
        // osztályszintű (statikus) adattagok
        static Random rnd;
        static int peldanyDb;

        // osztályszintű tulajdonság
        public static int PeldanyDb { get { return peldanyDb; } }

        // osztályszintű konstruktor
        // nincs láthatósági jelző (access modifiers are not allowed on static constructors)
        static Sutemeny()
        {
            rnd = new Random();
            peldanyDb = 0;
        }



        // példányszintű adattag
        int ar;

        // konstruktor
        public Sutemeny()
        {
            ar = rnd.Next(290,9991);
            peldanyDb++;
            Console.WriteLine("egy új süti elkészítve, összesen: " + peldanyDb);
        }


        // destruktor
        ~Sutemeny() // AltGr + 1: ~
        {
            peldanyDb--;
            Console.WriteLine("megettem egy süteményt, maradt ennyi: " + peldanyDb);
        }
    }
}

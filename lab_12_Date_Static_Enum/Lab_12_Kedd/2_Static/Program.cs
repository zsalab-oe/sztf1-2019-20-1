﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Static
{
    class Program
    {
        static void Main(string[] args)
        {
            // statikus adattag elérése az osztályon keresztül lehetséges
            //OsztályNeve.Tulajdonság vagy OsztályNeve.Metódus()

            Console.WriteLine(Sutemeny.PeldanyDb);


            //Sutemeny[] sutik = new Sutemeny[10];
            //for (int i = 0; i < sutik.Length; i++)
            //{
            //    sutik[i] = new Sutemeny();
            //}

            //Console.WriteLine(Sutemeny.PeldanyDb);

            //Sutemeny suti1 = new Sutemeny();
            //Sutemeny suti2 = new Sutemeny();

            //Console.WriteLine(Sutemeny.PeldanyDb);

            //suti1 = null;
            //suti2 = null;
            

            // ****************************************************
            // elérhetetlen memóriaterületek generálása azzal, hogy létrehozzuk az objektumokat, de egyből megszüntetjük a rájuk mutató hivatkozásokat (maguk az objektumok ilyenkor még bent maradnak a memóriában)
            for (int i = 0; i < 10; i++)
            {
                Sutemeny suti = new Sutemeny();
            }

            Console.WriteLine(Sutemeny.PeldanyDb);

            // memóriafelszabadítás "kikényszerítése" Garbage Collector (szemétgyűjtő) segítségével
            GC.Collect();

            Console.WriteLine("Ez vajon a szemétgyűjtés után fut le?");



            //Matek m = new Matek(); // hibás: statikus osztályt nem lehet példányosítani
            Console.WriteLine(Matek.KorKerulete(10));


            Console.ReadLine();
        }
    }


    // statikus osztály
    // - csak statikus tagokat tartalmazhat
    // - nem lehet példányosítani
    static class Matek
    {
        static double pi;

        static Matek()
        {
            // may I have a large container of coffee :)
            pi = 3.1415926;
        }

        public static double KorKerulete(double sugar)
        {
            return 2 * sugar * Math.PI;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egyszeru_prog_tetelek
{
    class Program
    {
        // Azért itt hozom létre, hogy elfogadható véletlenszámokat generáljon.
        static Random rnd = new Random();

        static void Main(string[] args)
        {
            //Random rnd = new Random();

            int[] A = new int[10];
            for (int i = 0; i < A.Length; i++)
            {
                A[i] = rnd.Next(0, 101);
            }

            for (int i = 0; i < A.Length; i++)
            {
                Console.Write(A[i] + ", ");
            }
            Console.WriteLine();


            /* Sorozatszámítás tétel */
            int osszeg = 0;
            for (int i = 0; i < A.Length; i++)
            {
                osszeg += A[i];
            }
            Console.WriteLine("Összeg: " + osszeg);


            /* Eldöntés tétel */
            bool van;
            int j = 0;
            while(j < A.Length && A[j] % 5 != 0)
            {
                j++;
            }
            if (j < A.Length)
                van = true;
            else
                van = false;
            Console.WriteLine("Van öttel osztható. " + van);

            if (van)
            {
                /* Kiválasztás tétel */
                j = 0;
                while(A[j] % 5 != 0)
                {
                    j++;
                }
                Console.WriteLine("Az első 5-tel osztható elem indexe: " + j);
            }

            /* Lineáris keresés tétel */
            j = 0;
            van = false;
            while (j < A.Length && A[j] % 5 != 0)
            {
                j++;
            }
            if (j < A.Length)
                van = true;
            else
                van = false;
            int index;
            if (van)
                index = j;
            else
                index = -1;
            Console.WriteLine("Az első 5-tel osztható elem indexe (lineáris keresés): " + index);


            /* Megszámlálás tétel tétel */
            int db = 0;
            for (int i = 0; i < A.Length; i++)
            {
                if (A[i] % 5 == 0)
                    db++;
            }
            Console.WriteLine("Öttel oszthatók darabszáma: " + db);

            /* Maximum-, Minimumkiválasztás tétel */
            int minIndex = 0;
            for (int i = 1; i < A.Length; i++)
            {
                if (A[i] < A[minIndex])
                    minIndex = i;
            }
            Console.WriteLine("A legkisebb elem indexe: " + minIndex);

            Console.WriteLine("A legkisebb elem: " + A[minIndex]);



            Console.ReadLine();
        }
    }
}

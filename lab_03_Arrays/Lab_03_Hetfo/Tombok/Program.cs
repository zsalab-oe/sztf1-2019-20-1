﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tombok
{
    class Program
    {
        static void Main(string[] args)
        {
            // deklaráció:
            // típus[] tömbnév;
            int[] adatok;

            // tömblétrehozás:
            // tömbnév = new típus[elemszám];
            adatok = new int[10];

            // Hivatkozás a tömb egy elemére:
            //     tömbnév[index]
            // ahol index egy nemnegatív egész szám (index >= 0)
            adatok[0] = 5;
            adatok[4] = 15;
            // Ertékadás hiányában a tömbelem a típus alapértékét veszi fel Ez int típusnál: 0

            // A tömb első elemének indexe: 0
            // A tömb utolsó elemének indexe: elemszám - 1
            // Kisebb, vagy nagyobb index megadása futási hibát okoz
            Console.WriteLine("A tömb első eleme: " + adatok[0]);
            Console.WriteLine("A tömb utolsó eleme: " + adatok[adatok.Length - 1]);

            //hibás:
            //Console.WriteLine(adatok[-1]);
            //Console.WriteLine(adatok[adatok.Length]);


            int j = 0;
            while (j < adatok.Length)
            {
                Console.WriteLine(adatok[j]);
                j++;
            }

            Console.WriteLine("--------------");

            for (int i = 0; i < adatok.Length; i++)
            {
                Console.WriteLine(adatok[i]);
            }

            for (int i = 0, k = 1; i < adatok.Length; i++, k = k << 1)
            {
                adatok[i] = k;
            }

            Console.WriteLine("================");
            for (int i = 0; i < adatok.Length; i++)
            {
                Console.WriteLine(adatok[i]);
            }

            bool[] flags = new bool[6];
            flags[2] = true;
            Console.WriteLine(false || flags[2]);


            int[] adatokMasolata = adatok;

            adatok[3] = -17;
            for (int i = 0; i < adatokMasolata.Length; i++)
            {
                Console.Write(adatokMasolata[i] + ", ");
            }

            int[] tenylegesMasolat = new int[adatok.Length];
            for (int i = 0; i < adatok.Length; i++)
            {
                tenylegesMasolat[i] = adatok[i];
            }


            Console.ReadLine();
        }
    }
}
;
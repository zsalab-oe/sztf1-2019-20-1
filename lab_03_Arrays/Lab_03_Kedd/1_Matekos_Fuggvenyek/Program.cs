﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Matekos_Fuggvenyek
{
    class Program
    {
        static void Main(string[] args)
        {
            int a = 5;
            int b = 7;
            int c = Math.Min(a, b);
            Console.WriteLine(c);

            double r1 = 14;
            double terulet1 = Math.Pow(r1, 2) * Math.PI;

            double r2 = 16;
            double terulet2 = Math.Pow(r2, 2) * Math.PI;

            Console.WriteLine(terulet2 / terulet1 * 100);


            Console.ReadLine();
        }
    }
}

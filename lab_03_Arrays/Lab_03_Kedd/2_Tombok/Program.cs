﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Tombok
{
    class Program
    {
        static void Main(string[] args)
        {
            // deklaráció:
            // típus[] tömbnév;
            int[] adatok;

            // tömblétrehozás:
            // tömbnév = new típus[elemszám];
            adatok = new int[10];

            // Hivatkozás a tömb egy elemére:
            //     tömbnév[index]
            // ahol index egy nemnegatív egész szám (index >= 0)
            adatok[2] = 5;
            adatok[7] = 14;
            // Ertékadás hiányában a tömbelem a típus alapértékét veszi fel Ez int típusnál: 0

            Console.WriteLine(adatok[2]);
            Console.WriteLine(adatok[3]);
            
            // A tömb első elemének indexe: 0
            // A tömb utolsó elemének indexe: elemszám - 1
            // Kisebb, vagy nagyobb index megadása futási hibát okoz
            Console.WriteLine("A tömb első eleme: " + adatok[0]);
            Console.WriteLine("A tömb utolsó eleme: " + adatok[adatok.Length - 1]);

            //hibás:
            //Console.WriteLine(adatok[-1]);
            //Console.WriteLine(adatok[adatok.Length]);
            
            //Console.WriteLine(adatok);


            bool[] flags = new bool[6];
            flags[3] = true;
            Console.WriteLine(false || flags[3]);

            int[] adatokMasolata = adatok;
            adatokMasolata[2] = 100;

            Console.WriteLine(adatok[2]);

            int[] tenylegesMasolat = new int[adatok.Length];


            for (int i = 0; i < adatok.Length; i++)
            {
                adatok[i] = i * 10;
            }

            for (int i = 0; i < adatok.Length; i++)
            {
                Console.Write(adatok[i] + ", ");
            }
            Console.WriteLine();

            int j = 0;
            while (j < adatok.Length)
            {
                Console.Write(adatok[j] + ", ");
                j++;
            }
            Console.WriteLine();

            for (int i = 0, k = 1; i < adatok.Length; i++, k = k << 1)
            {
                adatok[i] = k;
            }

            for (int i = 0; i < adatok.Length; i++)
            {
                Console.Write(adatok[i] + ", ");
            }
            Console.WriteLine();



            Console.ReadLine();
        }
    }
}

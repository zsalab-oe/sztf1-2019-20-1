﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Metodusok
{
    class Program
    {
        static void Main(string[] args)
        {
            Kiir("Ezt írd ki!", 4);
            Kiir("Most meg ezt!", 3);
            double a = Duplaz(3);
            Console.WriteLine(a);
            double b = 4.6;
            int c = (int)Duplaz(b);
            Console.WriteLine(c);

            Console.WriteLine(Szamol(3));
            Console.WriteLine(Szamol(5, 3));

            Console.WriteLine("\n==========");


            // címszerinti paraméterátadás
            //   - egy memóriacím kerül átadásra, esetünkben az x változó memóriacíme
            //   - jelölés: ref kulcsszóval, a címszerint bekért / átadni kívánt paraméter(ek) előtt
            //   - jelölni kell a metódus fejlécében ÉS a metódus hívásánál is
            int x = 10;
            Console.WriteLine(x);
            Novel(ref x);
            Console.WriteLine(x);

            Console.WriteLine("\n==========");

            // Metódus túlterhelés (method overloading)
            // - metódusok megegyező névvel, de eltérő szignatúrával
            Kiir("szöveg");
            Kiir("szöveg 2", 3);

            Console.ReadLine();
        }


        #region Metódus túlterhelés

        static void Kiir(string szoveg, int db)
        {
            for (int i = 0; i < db; i++)
            {
                Console.WriteLine(szoveg);
            }
        }

        static void Kiir(string szoveg)
        {
            Console.WriteLine(szoveg);
        } 

        #endregion


        static double Duplaz(double x)
        {
            double eredmeny = 2 * x;
            return eredmeny;
        }

        static int Szamol(int max, int min = 0) // az opcionális paraméterek csak a kötelező paraméterek után következhetnek
        {
            int osszeg = 0;
            for (int i = min; i < max; i++)
            {
                osszeg += i;
            }
            return osszeg;
        }

        static void Novel(ref int szam)
        {
            szam++;
            Console.WriteLine(">> szám növelt értéke: " + szam);
        }
    }
}

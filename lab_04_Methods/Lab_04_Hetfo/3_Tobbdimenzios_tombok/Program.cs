﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Tobbdimenzios_tombok
{
    class Program
    {
        static void Main(string[] args)
        {
            // deklaráció:
            // típus[vesszők] tömbnév; (a szögletes zárójelbe dimenziószám-1 darab vesszőt kell tenni)
            // tömblétrehozás:
            // tömbnév = new típus[elemszám1,...,elemszámN]; (az egyes dimenziók elemszámait vesszőkkel elválasztva kell megadni)
            // A deklaráció és a tömblétrehozás itt is összevonható:
            int[,] matrix = new int[4, 7];
            Random rnd = new Random();
            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                for (int j = 0; j < matrix.GetLength(1); j++)
                {
                    matrix[i, j] = rnd.Next(0, 101);
                }
            }
            Console.WriteLine(TombSzovegkent(matrix));


            Console.ReadLine();
        }

        static string TombSzovegkent(int[,] tomb)
        {
            string s = "";
            for (int i = 0; i < tomb.GetLength(0); i++)
            {
                for (int j = 0; j < tomb.GetLength(1); j++)
                {
                    s += tomb[i, j] + "\t"; // \t jelentése: tabulátor
                }
                s += "\n"; // \n jelentése: sortörés
            }
            return s;
        }
    }
}

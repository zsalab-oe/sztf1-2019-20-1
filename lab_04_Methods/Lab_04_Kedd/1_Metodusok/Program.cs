﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Metodusok
{
    class Program
    {
        static void Main(string[] args)
        {
            Kiir("Ezt írd ki!", 5);
            double ertek = Duplaz(4.2);
            Console.WriteLine(ertek);

            // Metódus túlterhelés (method overloading)
            // - metódusok megegyező névvel, de eltérő szignatúrával
            Bemutatkozas("Béla");
            Bemutatkozas("Józsi", 24);
            Bemutatkozas(47);

            Szamol(10, 4);
            Szamol(10);

            Console.WriteLine("--------");


            // címszerinti paraméterátadás
            //   - egy memóriacím kerül átadásra, esetünkben az x változó memóriacíme
            //   - jelölés: ref kulcsszóval, a címszerint bekért / átadni kívánt paraméter(ek) előtt
            //   - jelölni kell a metódus fejlécében ÉS a metódus hívásánál is
            int x = 10;
            Console.WriteLine(x);
            Novel(ref x);
            Console.WriteLine(x);

            Console.ReadLine();
        }

        static void Kiir(string szoveg, int db)
        {
            for (int i = 0; i < db; i++)
            {
                Console.WriteLine(szoveg);
            }
        }

        static double Duplaz(double x)
        {
            double eredmeny = 2 * x;
            return eredmeny;
        }

        #region Metódus túlterhelés

        static void Bemutatkozas(string nev)
        {
            Console.WriteLine("Szia, " + nev + " vagyok!");
        }

        static void Bemutatkozas(string nev, int eletkor)
        {
            Console.WriteLine("Szia, " + nev + " vagyok, " + eletkor + " éves.");
        }

        static void Bemutatkozas(int eletkor)
        {
            Console.WriteLine("Szia, " + eletkor + " éves vagyok.");
        } 

        #endregion

        static void Szamol(int end, int start = 0) // az opcionális paraméterek csak a kötelező paraméterek után következhetnek
        {
            for (int i = start; i < end; i++)
            {
                Console.Write(i + " ");
            }
            Console.WriteLine();
        }

        static void Novel(ref int szam)
        {
            szam++;
            Console.WriteLine(">> szám növelt értéke: " + szam);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Tobbdimenzios_tombok
{
    class Program
    {
        static void Main(string[] args)
        {
            // deklaráció:
            // típus[vesszők] tömbnév; (a szögletes zárójelbe dimenziószám-1 darab vesszőt kell tenni)
            // tömblétrehozás:
            // tömbnév = new típus[elemszám1,...,elemszámN]; (az egyes dimenziók elemszámait vesszőkkel elválasztva kell megadni)
            // A deklaráció és a tömblétrehozás itt is összevonható:
            int[,] matrix = new int[4,7];
            Console.WriteLine("Sorok száma: " + matrix.GetLength(0));
            Console.WriteLine("Oszlopok száma: " + matrix.GetLength(1));

            matrix = TombLetrehozas(5, 10);
            Console.WriteLine(TombSzovegkent(matrix));
            Console.WriteLine("A 2. oszlop elemeinek összege: " + OszlopOsszeg(matrix, 1));

            Console.ReadLine();
        }

        static int[,] TombLetrehozas(int m, int n)
        {
            int[,] tomb = new int[m, n];
            for (int i = 0; i < tomb.GetLength(0); i++)
            {
                for (int j = 0; j < tomb.GetLength(1); j++)
                {
                    tomb[i, j] = 10 * i + j;
                }
            }
            return tomb;
        }

        static string TombSzovegkent(int[,] tomb)
        {
            string s = "";
            for (int i = 0; i < tomb.GetLength(0); i++)
            {
                for (int j = 0; j < tomb.GetLength(1); j++)
                {
                    s += tomb[i, j] + "\t"; // \t jelentése: tabulátor
                }
                s += "\n"; // \n jelentése: sortörés
            }
            return s;
        }

        static int OszlopOsszeg(int[,] tomb, int oszlop)
        {
            int osszeg = 0;
            for (int i = 0; i < tomb.GetLength(0); i++)
            {
                osszeg += tomb[i, oszlop];
            }
            return osszeg;
        }
    }
}

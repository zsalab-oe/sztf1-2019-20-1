﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Foreach
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tomb = new int[] { 4, 67, 1, 2, 4, 0, 4, 6};

            for (int i = 0; i < tomb.Length; i++)
            {
                Console.WriteLine(tomb[i]);
                tomb[i] = 2 * tomb[i];
            }

            foreach (int ertek in tomb)
            {
                Console.WriteLine(ertek);
                //ertek = 2 * ertek; // hibás, az iterációs változó (ertek) nem módosítható
            }
        }
    }
}

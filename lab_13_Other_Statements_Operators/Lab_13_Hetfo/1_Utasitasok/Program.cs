﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Utasitasok
{
    enum MenuPont { Felvetel = 1, Listaz, Stats, Kilepes }
    class Program
    {
        static void Main(string[] args)
        {
            MenuPont valasztott;
            do
            {
                MenuKiir();
                valasztott = (MenuPont)int.Parse(Console.ReadLine());

                switch (valasztott)
                {
                    case MenuPont.Felvetel:
                        Console.WriteLine("Felvétel menüpont választva...");
                        break;
                    case MenuPont.Listaz:
                        Console.WriteLine("Listázás menüpont választva...");
                        break;
                    case MenuPont.Stats:
                        Console.WriteLine("Statisztikák menüpont választva...");
                        break;
                    case MenuPont.Kilepes:
                        Console.WriteLine("Kilépés menüpont választva...");
                        break;
                    default:
                        Console.WriteLine("Nincs ilyen menüpont.");
                        break;
                }
                Console.ReadLine();

            } while (valasztott != MenuPont.Kilepes);
        }

        static void MenuKiir()
        {
            Console.Clear();
            Console.WriteLine("1. Felvetel");
            Console.WriteLine("2. Listáz");
            Console.WriteLine("3. Statisztikák");
            Console.WriteLine("4. Kilépés");
        }


    }
}

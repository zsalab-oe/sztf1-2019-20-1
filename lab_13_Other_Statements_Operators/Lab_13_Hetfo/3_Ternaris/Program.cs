﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Ternaris
{
    class Szemely
    {
        public string Koszones()
        {
            return "Szia";
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            /* Ternáris operátor ?: */
            // általános forma:
            // valami = (logikai feltétel) ? érték_ha_igaz : érték_ha_hamis;
            // csak egyenlőségjel jobb oldalán állhat

            int x = 5;
            string oszthatosag;
            if (x % 2 == 0)
                oszthatosag = "páros";
            else
                oszthatosag = "páratlan";

            // ugyanez:
            oszthatosag = (x % 2 == 0) ? "páros" : "páratlan";


            /* ? operátor */
            // null ellenőrzés egyszerűsítésére szolgál

            Szemely pista = null;
            //pista = new Szemely();
            if (pista != null)
            {
                Console.WriteLine(pista.Koszones());
            }
            else
            {
                Console.WriteLine();
            }

            // ugyanez:
            Console.WriteLine(pista?.Koszones());
        }
    }
}

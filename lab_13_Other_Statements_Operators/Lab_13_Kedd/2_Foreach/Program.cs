﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Foreach
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] tomb = new int[] { 4, 6, 12, 0, 6, 34 };

            for (int i = 0; i < tomb.Length; i++)
            {
                Console.WriteLine(tomb[i]);
                tomb[i] = 2 * tomb[i];
            }

            foreach (int elem in tomb)
            {
                Console.WriteLine(elem);
                //elem = 2 * elem; // hibás, az iterációs változó (elem) nem módosítható
            }
        }
    }
}

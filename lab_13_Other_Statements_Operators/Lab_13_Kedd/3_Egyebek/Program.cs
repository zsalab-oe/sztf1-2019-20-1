﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Egyebek
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ternáris operátor ?: */
            // általános forma:
            // valami = (logikai feltétel) ? érték_ha_igaz : érték_ha_hamis;
            // csak egyenlőségjel jobb oldalán állhat

            int x = 5;
            string oszthatosag;

            if (x % 2 == 0)
                oszthatosag = "páros";
            else
                oszthatosag = "páratlan";

            // ugyanez:
            oszthatosag = (x % 2 == 0) ? "páros" : "páratlan";
            



            /* ? operátor */
            // null ellenőrzés egyszerűsítésére szolgál

            Szemely[] szemelyek = new Szemely[5];
            szemelyek[0] = new Szemely();
            szemelyek[1] = new Szemely();
            szemelyek[3] = new Szemely();

            for (int i = 0; i < szemelyek.Length; i++)
            {
                if (szemelyek[i] != null)
                    szemelyek[i].Koszones();
            }

            Console.WriteLine();

            // ugyanez:
            for (int i = 0; i < szemelyek.Length; i++)
            {
                szemelyek[i]?.Koszones();
            }

            Console.WriteLine();

            // ugyanez foreach ciklussal
            foreach (Szemely sz in szemelyek)
            {
                sz?.Koszones();
            }

            Console.ReadLine();
        }
    }

    class Szemely
    {
        public void Koszones()
        {
            Console.WriteLine("Szia");
        }
    }
}

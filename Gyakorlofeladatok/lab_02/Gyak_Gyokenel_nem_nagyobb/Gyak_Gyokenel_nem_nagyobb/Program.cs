﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_Gyokenel_nem_nagyobb
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("x: ");
            float x = float.Parse(Console.ReadLine());
            int a = 1;
            while (a * a <= x)
            {
                a++;
            }
            Console.WriteLine("A gyökénél nem nagyobb, legnagyobb értékű pozitív egész szám: " + --a);

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_Osszes_oszto
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());
            Console.WriteLine(n + " osztói:");
            int j = 1;
            while (j <= n / 2)
            {
                if (n % j == 0)
                    Console.WriteLine(j);
                j++;
            }
            Console.WriteLine(n);


            /* Kicsit haladóbb verzió: */
            //Console.Write("m: ");
            //int m = int.Parse(Console.ReadLine());
            //Console.WriteLine(m + " osztói:");
            //int k = 1;
            //while (k * k <= m)
            //{
            //    if (m % k == 0)
            //    {
            //        Console.WriteLine(k);
            //        int i = m / k;
            //        if (k != i)
            //            Console.WriteLine(i);
            //    }
            //    k++;
            //}


            Console.ReadLine();
        }
    }
}

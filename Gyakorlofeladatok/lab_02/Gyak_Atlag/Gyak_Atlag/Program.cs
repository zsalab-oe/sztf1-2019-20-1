﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_Atlag
{
    class Program
    {
        static void Main(string[] args)
        {
            // az egyszerűség kedvéért legalább 3 pozitív értéket kérünk be, mielőtt elfogadnánk egy nem pozitívat kilépési eseményként

            int bekertSzam;
            int osszeg = 0;
            int db = 0;
            int min = int.MaxValue;
            int max = 0;
            do
            {
                Console.Write("Következő szám: ");
                bekertSzam = int.Parse(Console.ReadLine());
                if (bekertSzam > 0)
                {
                    osszeg += bekertSzam;
                    db++;
                    if (bekertSzam < min || min == 0)
                        min = bekertSzam;
                    if (bekertSzam > max)
                        max = bekertSzam;
                }
            } while (db <= 2 || bekertSzam > 0);
            osszeg = osszeg - min - max;
            db = db - 2;
            float atlag = (float)osszeg / db;
            Console.WriteLine("Átlag: " + atlag);


            Console.ReadLine();
        }
    }
}

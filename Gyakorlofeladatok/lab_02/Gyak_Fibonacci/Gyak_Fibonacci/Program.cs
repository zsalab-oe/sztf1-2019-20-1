﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Gyak_Fibonacci
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("n: ");
            int n = int.Parse(Console.ReadLine());

            if (n <= 1)
            {
                Console.WriteLine($"Fibonacci({n}) = {n}");
            }
            else
            {
                int eredm = 0;
                int elozo = 1;
                int elozoElotti = 0;
                int j = 2;
                while (j <= n)
                {
                    eredm = elozo + elozoElotti;
                    elozoElotti = elozo;
                    elozo = eredm;
                    j++;
                }
                Console.WriteLine($"Fibonacci({n}) = {eredm}");
            }

            Console.ReadLine();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Memes
{
    class Meme
    {
        Szerzo szerzo;
        int pontszam;
        string kategoria;


        public Szerzo Szerzo { get { return szerzo; } }
        public int Pontszam { get { return pontszam; } }


        public Meme(Szerzo szerzo, string kategoria)
        {
            this.szerzo = szerzo;
            this.kategoria = kategoria;
            pontszam = 0;
        }


        public void PontotKap(int kapottPont)
        {
            pontszam += kapottPont;
            if (kapottPont == 1)
                szerzo.FeketePontotKap();
        }
    }
}

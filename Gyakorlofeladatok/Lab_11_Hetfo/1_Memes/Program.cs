﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.IO;

namespace _1_Memes
{
    class Program
    {
        static void Main(string[] args)
        {
            Szerzo sz1 = new Szerzo("Aladár");
            Szerzo sz2 = new Szerzo("Béla");
            Szerzo sz3 = new Szerzo("Cecil");

            int[] szamok = new int[] { 4, 5, 6, 10, -5 };

            string[] kategoriak = new string[] { "OC", "Disszipáció", "Star Wars", "Repost"};

            Meme elsoMeme = new Meme(sz1, kategoriak[0]);

            Meme[] bekuldottMemek = new Meme[]
            {
                elsoMeme,
                new Meme(sz2, kategoriak[1]),
                new Meme(sz3, kategoriak[3]),
                new Meme(sz1, kategoriak[2]),
                new Meme(sz2, kategoriak[0]),
                new Meme(sz2, kategoriak[2])
            };

            MemeCsoport csoport = new MemeCsoport(bekuldottMemek, 7);

            Random rnd = new Random();
            csoport.Ertekel(rnd);

            Szerzo[] szerzok = new Szerzo[] { sz1, sz2, sz3 };
            
            SzerzoListaz(szerzok);

            Szerzo legjobbMemeSzerzoje = csoport.LegjobbSzerzo();
            Console.WriteLine("\nLegjobb meme szerzője: " + legjobbMemeSzerzoje.Nev);
            File.WriteAllText("legjobb.txt", legjobbMemeSzerzoje.Nev);


            Szerzo[] kitiltottak = csoport.Kitiltottak();
            Console.WriteLine("\nKitiltottak:");
            SzerzoListaz(kitiltottak);
            SzerzoListaz(kitiltottak, "kitiltottak.txt");


            Console.ReadLine();
        }

        static void SzerzoListaz(Szerzo[] szerzok)
        {
            for (int i = 0; i < szerzok.Length; i++)
            {
                string adatok = szerzok[i].Megjelenit();
                Console.WriteLine(adatok);
            }
        }

        static void SzerzoListaz(Szerzo[] szerzok, string eleresiUt)
        {
            StreamWriter sw = new StreamWriter(eleresiUt);
            for (int i = 0; i < szerzok.Length; i++)
            {
                string adatok = szerzok[i].Megjelenit();
                sw.WriteLine(adatok);
            }
            sw.Close();
        }
    }
}

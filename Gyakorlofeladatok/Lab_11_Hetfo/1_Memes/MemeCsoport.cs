﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Memes
{
    class MemeCsoport
    {
        Meme[] memes;
        int adminokSzama; // memeTanacsDb


        public MemeCsoport(Meme[] memes, int adminokSzama)
        {
            this.memes = memes;
            this.adminokSzama = adminokSzama;
        }



        public void Ertekel(Random rnd)
        {
            for (int i = 0; i < adminokSzama; i++)
            {
                for (int j = 0; j < memes.Length; j++)
                {
                    int generaltPontszam = rnd.Next(1, 6);
                    memes[j].PontotKap(generaltPontszam);
                }
            }
        }

        public Szerzo LegjobbSzerzo()
        {
            int legjobbMeme = 0;
            for (int i = 1; i < memes.Length; i++)
            {
                if (memes[i].Pontszam > memes[legjobbMeme].Pontszam)
                    legjobbMeme = i;
            }
            Szerzo szerzo = memes[legjobbMeme].Szerzo;
            return szerzo;
        }

        bool BenneVan(Szerzo[] szerzok, Szerzo kerdeses)
        {
            int j = 0;
            while (j < szerzok.Length && szerzok[j] != null && szerzok[j] != kerdeses)
            {
                j++;
            }
            bool benneVan = j < szerzok.Length && szerzok[j] != null;
            return benneVan;
        }

        public Szerzo[] Kitiltottak()
        {
            Szerzo[] kitiltottak = new Szerzo[memes.Length];
            int db = 0;
            for (int i = 0; i < memes.Length; i++)
            {
                Szerzo aktSzerzo = memes[i].Szerzo;
                if (aktSzerzo.Kitiltva && !BenneVan(kitiltottak, aktSzerzo))
                {
                    kitiltottak[db] = aktSzerzo;
                    db++;
                }
            }

            Szerzo[] vissza = new Szerzo[db];
            for (int i = 0; i < db; i++)
            {
                vissza[i] = kitiltottak[i];
            }
            return vissza;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Memes
{
    class Szerzo
    {
        string nev;
        int feketePontDb;

        public string Nev { get { return nev; } }
        public bool Kitiltva
        {
            get
            {
                //if (feketePontDb >= 3)
                //    return true;
                //else
                //    return false;
                return feketePontDb >= 3;
            }
        }


        public Szerzo(string nev)
        {
            this.nev = nev;
            feketePontDb = 0;
        }



        public void FeketePontotKap()
        {
            feketePontDb++;
        }

        public string Megjelenit()
        {
            return $"{nev}, {feketePontDb} fekete pont";
        }
    }
}

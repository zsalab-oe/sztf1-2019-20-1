﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Minta_ZH_1_GB_mego
{
    class Program
    {
        static void Main(string[] args)
        {
            string adatsor = "Ausztrália;8;11;10#Dél-Korea;9;3;9#Egyesült Államok;46;37;38#Franciaország;10;18;14#Japán;12;8;21#Kína;26;18;26#Nagy-Britannia;27;23;17#Németország;17;10;15#Olaszország;8;12;8#Oroszország;19;17;20";
            string europaiOrszagokFelsorolas = "Albánia;Andorra;Ausztria;Azerbajdzsán;Belgium;BoszniaHercegovina;Bulgária;Ciprus;Csehország;Dánia;Nagy-Britannia;Észtország;Fehéroroszország;Finnország;Franciaország;Görögország;Grúzia;Hollandia;Horvátország;Írország;Izland;Kazahsztán;Lengyelország;Lettország;Liechtenstein;Litvánia;Luxemburg;Macedónia;Magyarország;Málta;Moldova;Monaco;Montenegró;Németország;Norvégia;Olaszország;Oroszország;Örményország;Portugália;Románia;SanMarino;Spanyolország;Svájc;Svédország;Szerbia;Szlovákia;Szlovénia;Törökország;Ukrajna;Vatikán";
            
            string[] europaiOrszagok = null;
            string[] orszagok = null;
            int[,] ermek = null;

            // Split() stringfüggvény:
            //   - string feldarabolása egy megadott karakter mentén: forrásVáltozó.Split(karakter), visszatérési értéke egy string tömb
            //   - a tömb elemszáma = keresett karakter darabszáma a szövegben + 1
            europaiOrszagok = europaiOrszagokFelsorolas.Split(';');
            Feltolt(adatsor, ref orszagok, ref ermek);


            Console.WriteLine("Van ország 0 darab arany, ezüst vagy bronzéremmel? " + ValamilyenEremNelkul(ermek) + "\n");

            int[] indexek = EremKereses(ermek, 500);
            Console.WriteLine("Keresés eredménye (sor, oszlop koordináták, -1, -1, ha nincs): " + indexek[0] + ", " + indexek[1] + "\n");

            int legkevesebbEzustIndex = LegkevesebbEzust(ermek);
            Console.WriteLine("A legkevesebb ezüstérmet gyűjtő ország: " + orszagok[legkevesebbEzustIndex] + "\n");

            Console.WriteLine("Legalább 10 aranyat szereztek:");
            string[] legalabbTizArany = LegalabbTizAranyatSzerzok(orszagok, ermek);
            TombKiir(legalabbTizArany);

            int[] ermekOrszagonkent = ErmekSzamaOrszagonkent(ermek);
            string[] orszagokRendezettErmekSzerint = RendezesErmekSzerint(orszagok, ermekOrszagonkent);
            TombKiir(orszagokRendezettErmekSzerint);

            int magyarHelyezes = MagyarHelyezes(orszagokRendezettErmekSzerint) + 1;
            Console.WriteLine("Magyarország helye a rangsorban (0, ha nincs benne az első 10-ben): " + magyarHelyezes + "\n");


            Console.WriteLine("Európai országok a top 10-ben:");
            string[] europaiakTopTizben = LegjobbEuropaiOrszagok(orszagok, europaiOrszagok);
            TombKiir(europaiakTopTizben);

            Console.ReadLine();
        }

        static void TombKiir(string[] tomb)
        {
            for (int i = 0; i < tomb.Length; i++)
                Console.WriteLine(tomb[i]);
            Console.WriteLine();
        }

        static void Feltolt(string adatsor, ref string[] orszagok, ref int[,] ermek)
        {
            string[] adatok = adatsor.Split('#');
            orszagok = new string[adatok.Length];
            ermek = new int[adatok.Length, 3];
            for (int i = 0; i < adatok.Length; i++)
            {
                string[] orszagAdatok = adatok[i].Split(';');
                orszagok[i] = orszagAdatok[0];
                for (int j = 0; j < ermek.GetLength(1); j++)
                {
                    ermek[i, j] = int.Parse(orszagAdatok[j + 1]);
                }
            }
        }

        static bool ValamilyenEremNelkul(int[,] ermek)
        {
            int i = 0;
            bool talalt = false;
            while (i < ermek.GetLength(0) && !talalt)
            {
                int j = 0;
                while (j < ermek.GetLength(1) && ermek[i, j] != 0)
                {
                    j++;
                }
                if (j < ermek.GetLength(1))
                    talalt = true;
                else
                    i++;
            }
            return talalt;
        }

        static int[] EremKereses(int[,] ermek, int keresettErtek = 3)
        {
            int i = 0;
            int j = 0;
            bool talalt = false;
            while (i < ermek.GetLength(0) && !talalt)
            {
                j = 0;
                while (j < ermek.GetLength(1) && ermek[i, j] != keresettErtek)
                {
                    j++;
                }
                if (j < ermek.GetLength(1))
                    talalt = true;
                else
                    i++;
            }
            if (talalt)
                return new int[] { i, j };
            else
                return new int[] { -1, -1 };
        }

        static int LegkevesebbEzust(int[,] ermek)
        {
            int minIndex = 0;
            for (int i = 1; i < ermek.GetLength(0); i++)
            {
                if (ermek[i, 1] < ermek[minIndex, 1])
                    minIndex = i;
            }
            return minIndex;
        }

        static int OrszagOsszErem(int[,] ermek, int orszagIndex)
        {
            int osszeg = 0;
            for (int j = 0; j < ermek.GetLength(1); j++)
            {
                osszeg += ermek[orszagIndex, j];
            }
            return osszeg;
        }

        static int[] ErmekSzamaOrszagonkent(int[,] ermek)
        {
            int[] ermekOrszagonkent = new int[ermek.GetLength(0)];
            for (int i = 0; i < ermekOrszagonkent.Length; i++)
            {
                ermekOrszagonkent[i] = OrszagOsszErem(ermek, i);
            }
            return ermekOrszagonkent;
        }

        static int LegalabbTizAranyDb(int[,] ermek)
        {
            int db = 0;
            for (int i = 0; i < ermek.GetLength(0); i++)
            {
                if (ermek[i, 0] >= 10)
                    db++;
            }
            return db;
        }

        static string[] LegalabbTizAranyatSzerzok(string[] orszagok, int[,] ermek)
        {
            int db = LegalabbTizAranyDb(ermek);
            string[] legalabbTizAranyOrszagok = new string[db];
            db = 0;
            int i = 0;
            while (db < legalabbTizAranyOrszagok.Length)
            {
                if (ermek[i, 0] >= 10)
                {
                    legalabbTizAranyOrszagok[db] = orszagok[i];
                    db++;
                }
                i++;
            }
            return legalabbTizAranyOrszagok;
        }

        static string[] RendezesErmekSzerint(string[] orszagok, int[] ermekOrszagonkent)
        {
            // másolás
            //string[] masolat = orszagok; // <-- NEM jó megoldás, ezzel csak egy új változóval hivatkoznánk ugyanarra a tömbre, ami NEM ELÉG, mivel így a tömbelemek cserélgetésénél a bemeneti tömbökben cseréljük meg az elemek sorrendjét (ezt szeretnénk elkerülni)

            // JÓ megoldás: létre kell hoznunk 1-1 új tömböt, majd egyesével átmásolni az elemeket a bemeneti tömbökből
            string[] orszagokMasolata = new string[orszagok.Length];
            int[] ermekMasolata = new int[ermekOrszagonkent.Length];
            for (int i = 0; i < orszagok.Length; i++)
            {
                orszagokMasolata[i] = orszagok[i];
                ermekMasolata[i] = ermekOrszagonkent[i];
            }

            // rendezés (csökkenő sorrendben, hogy az első helyen legyen a legtöbb érmet szerző ország)
            for (int i = 0; i < ermekMasolata.Length - 1; i++)
            {
                int maxIndex = i;
                for (int j = i + 1; j < ermekMasolata.Length; j++)
                {
                    if (ermekMasolata[j] > ermekMasolata[maxIndex])
                        maxIndex = j;
                }
                // csere
                int temp1 = ermekMasolata[i];
                ermekMasolata[i] = ermekMasolata[maxIndex];
                ermekMasolata[maxIndex] = temp1;

                string temp2 = orszagokMasolata[i];
                orszagokMasolata[i] = orszagokMasolata[maxIndex];
                orszagokMasolata[maxIndex] = temp2;
            }

            for (int i = 0; i < orszagokMasolata.Length; i++)
            {
                orszagokMasolata[i] += " (" + ermekMasolata[i].ToString() + ")";
            }

            return orszagokMasolata;
        }

        static int MagyarHelyezes(string[] orszagokRendezve)
        {
            // Megjegyzés: a ciklusfeltételben Contains() stringfüggvényt használok, hogy a RendezesErmekSzerint() metódus által előállított tömböt használhassuk paraméterként, mivel ott 1-1 tömbelem nem csak egy országnevet tárol, hanem zárójelben az ország által szerzett érmek számát is
            int j = 0;
            while (j < orszagokRendezve.Length && !orszagokRendezve[j].Contains("Magyarország"))
            {
                j++;
            }
            if (j < orszagokRendezve.Length)
                return j;
            else
                return -1;
        }

        static string[] LegjobbEuropaiOrszagok(string[] orszagok, string[] europaiOrszagok) // metszet programozási tétel
        {
            string[] csakEuropai = new string[orszagok.Length];
            int db = 0;
            for (int i = 0; i < orszagok.Length; i++)
            {
                int j = 0;
                while (j < europaiOrszagok.Length && orszagok[i] != europaiOrszagok[j])
                {
                    j++;
                }
                if (j < europaiOrszagok.Length)
                {
                    csakEuropai[db] = orszagok[i];
                    db++;
                }
            }
            if (db == orszagok.Length) // ha mindegyik ország európai volt, nincs további teendőnk, visszaadjuk az országok tömböt
                return orszagok;
            else // egyéb esetben létrehozunk egy új tömböt csökkentett mérettel, átmásoljuk az európai országokat, és ezt a tömböt adjuk vissza
            {
                string[] csakEuropaiUj = new string[db];
                for (int i = 0; i < db; i++)
                {
                    csakEuropaiUj[i] = csakEuropai[i];
                }
                return csakEuropaiUj;
            }
        }
    }
}

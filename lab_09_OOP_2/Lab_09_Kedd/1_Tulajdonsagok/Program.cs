﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Tulajdonsagok
{
    class Program
    {
        static void Main(string[] args)
        {
            Hallgato jozsi = new Hallgato("AAA123", true);

            // értékadás, érték lekérdezése metódusokon keresztül
            jozsi.SetNeptunkod("HGE754");
            Console.WriteLine(jozsi.GetNeptunkod());

            // értékadás, érték lekérdezése tulajdonságon keresztül
            jozsi.Neptunkod = "ZQW456";
            Console.WriteLine(jozsi.Neptunkod);


            /* objektumtömb */

            Hallgato[] hallgatok = new Hallgato[]
                {
                    new Hallgato("AAA111", true),
                    new Hallgato("BBB222", true),
                    new Hallgato("CCC333", true),
                    new Hallgato("DDD444", true),
                    new Hallgato("EEE555", true),
                    new Hallgato("FFF666", true)
                };

            Listaz(hallgatok);
            Console.WriteLine("==============");

            // véletlenszerűen megváltoztatjuk néhány hallgató aktív státuszát
            Random rnd = new Random();
            for (int i = 0; i < hallgatok.Length; i++)
            {
                if (rnd.Next(0, 10) < 4)
                    hallgatok[i].AktivStatusz = false;
            }

            // töröljük a tömbből a nem aktív státuszú hallgatókat
            for (int i = 0; i < hallgatok.Length; i++)
            {
                if (!hallgatok[i].AktivStatusz)
                {
                    Console.WriteLine("Viszlát, " + hallgatok[i].Neptunkod + "!");
                    hallgatok[i] = null; // törlés megvalósítása: levesszük a hivatkozást az adott objektumról
                }
            }

            Console.WriteLine("==============");
            Listaz(hallgatok);

            // megpróbálunk felvenni egy új hallgatót egy üres helyre (ha van)
            if (Felvetel(jozsi, hallgatok))
            {
                Console.WriteLine("==============");
                Listaz(hallgatok);
            }

            


            Console.ReadLine();
        }

        static void Listaz(Hallgato[] hallgatok)
        {
            for (int i = 0; i < hallgatok.Length; i++)
            {
                // előfordulhat, hogy a tömb egyes elemeinek null az értéke (törölt hallgatók helyén)
                // null elemre nem lehet hivatkozni
                // ezért hozzáférés előtt ellenőrizzük, hogy érvényes memóriacímre hivatkozik-e a tömb i. eleme
                if (hallgatok[i] != null)
                    Console.WriteLine(hallgatok[i].Neptunkod + ", " + hallgatok[i].AktivStatusz);
            }
        }

        static bool Felvetel(Hallgato ujHallgato, Hallgato[] hallgatok)
        {
            int j = 0;
            while (j < hallgatok.Length && hallgatok[j] != null) // megkeressük az első üres helyet a tömbben
            {
                j++;
            }
            if (j < hallgatok.Length)
            {
                hallgatok[j] = ujHallgato;
                ujHallgato.AktivStatusz = true;
                return true; // a felvétel sikerült
            }
            else
                return false;  // a felvétel nem sikerült
        }
    }
}

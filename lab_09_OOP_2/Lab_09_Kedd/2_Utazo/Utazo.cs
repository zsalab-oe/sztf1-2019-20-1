﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Utazo
{
    class Utazo
    {
        // a könnyebben megvalósítható keresés érdekében úgy tárolom el a meglátogatott helyeket, hogy nem csak a városnevek között szerepel a ';' elválasztó karakter, hanem az egész karaktersorozat elején és végén is
        // pl.: ;Budapest;Pécs;Miskolc;Pécs;Zánka-Köveskál;Budapest;Győr;
        // így amikor keresünk a stringben, akkor nem egy városnevet keresünk, hanem egy ';' karakterekkel kiegészített (pl.: ";városnév;") karaktersorozatot
        // erre azért van szükség, hogy pl. a fenti karaktersorozatban a Contains() stringfüggvény "Zánka" állomásra keresve ne adjon igaz értéket (hiszen ott nem is járt az utazó, csak pár kilométerrel arrébb)

        private string helyek;
        public string[] HelyekTombkent { get { return helyek.Trim(';').Split(';'); } }

        // konstruktor generálás: Ctrl + .
        public Utazo(string helyek)
        {
            this.helyek = ";" + helyek + ";";
        }

        public Utazo()
        {
            this.helyek = ";";
        }

        public void Utazik(string hova)
        {
            helyek += hova + ";";
        }

        public bool JartE(string hol)
        {
            // a sajátos tárolási módnak köszönhetően nem magát a városnevet keresem a helyek stringben, hanem a ';' karakterekkel együtt vett karaktersorozatot, pl.: ";Debrecen;"
            string keresett = ";" + hol + ";";
            return helyek.Contains(keresett);
        }

        public string HolVoltElobb(string egyik, string masik)
        {
            int egyikIndex = helyek.IndexOf(";" + egyik + ";");
            int masikIndex = helyek.IndexOf(";" + masik + ";");
            if (egyikIndex == -1 && masikIndex == -1) // egyik helyen sem járt, ilyenkor üres stringet adunk vissza
                return "";
            else if (egyikIndex == -1) // az "egyik" helyen nem járt
                return masik;
            else if (masikIndex == -1) // a "másik" helyen nem járt
                return egyik;
            else if (egyikIndex < masikIndex) // mindkét helyen járt, megvizsgáljuk, melyik helyen járt előbb
                return egyik;
            else
                return masik;
        }

        public string[] HelyekIsmetlodesNelkul()
        {
            // a helyek stringhez hasonlóan az ismétlődésmentes stringben is plusz ';' karakterekkel tároljuk a városneveket, hiszen ebben is kell majd keresnünk

            string ismNelkul = ";";
            string[] helyekTomb = helyek.Trim(';').Split(';');
            for (int i = 0; i < helyekTomb.Length; i++)
            {
                if (!ismNelkul.Contains(";" + helyekTomb[i] + ";"))
                    ismNelkul += helyekTomb[i] + ";";
            }
            //string vagott = ismNelkul.Trim(';');
            //string[] vissza = vagott.Split(';');
            //return vissza;

            // az előző, kommentezett sorokkal ekvivalens:
            // először levágom az elejéről és a végéről a ';' karaktereket, majd a "maradékot" szétvágom egy string tömbbé a városnevek közötti ';' karakterek mentént, így előáll a kimeneti tömb
            return ismNelkul.Trim(';').Split(';');
        }

        public int HanyHelyenJart()
        {
            //string[] helyekIsmetlodesNelkul = HelyekIsmetlodesNelkul();
            //int helyekDb = helyekIsmetlodesNelkul.Length;
            //return helyekDb;

            // az előző, kommentezett sorokkal ekvivalens:
            return HelyekIsmetlodesNelkul().Length;
        }

        public int HanyHelyenJartProperty
        {
            get
            { return HelyekIsmetlodesNelkul().Length; }
        }

        // segéd metódus
        bool TobbszorJart(string[] helyekTomb, string hol)
        {
            int db = 0;
            int j = 0;
            while (db < 2 && j < helyekTomb.Length)
            {
                if (helyekTomb[j] == hol)
                    db++;
                j++;
            }
            return db == 2;
        }

        public string[] HelyekAholTobbszorJart()
        {
            string[] ismNelkul = HelyekIsmetlodesNelkul();
            string[] helyekTomb = helyek.Trim(';').Split(';');
            string tobbszor = "";
            for (int i = 0; i < ismNelkul.Length; i++)
            {
                if (TobbszorJart(helyekTomb, ismNelkul[i]))
                    tobbszor += ismNelkul[i] + ";";
            }
            return tobbszor.Trim(';').Split(';');
        }
    }
}

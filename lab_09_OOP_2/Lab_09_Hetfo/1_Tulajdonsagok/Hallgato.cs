﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Tulajdonsagok
{
    // kód automatikus formázása: Ctrl + K + D

    class Hallgato
    {
        //  **** LÁTHATÓSÁG ****

        // - a láthatóság határozza meg, hogy az osztály egyes tagjaira milyen széles körben hivatkozhatunk
        // PRIVATE: a privát láthatóság a legszigorúbb, ezzel a jelzővel ellátott tagok csak az osztályon belül hivatkozhatóak (tehát ebben az esetben a class Hallgato { } kódblokkjában), vagyis egy másik osztályból nem férünk hozzá ezekhez az adatokhoz
        // PUBLIC: publikus láthatóság, vagyis nincs megkötés arra vonatkozóan, honnan férhetünk hozzá az adathoz
        // vannak egyéb láthatósági jelzők is, ezekkel későbbi félévekben foglalkozunk
        // - amennyiben mi magunk nem adunk meg láthatóságot módosító kulcsszót, úgy az alapértelmezett láthatósági szint kerül használatba
        // - osztály tagjainak alapértelmezett láthatósága: PRIVATE




        // **** TULAJDONSÁGOK ****
        
        // az adattagokat tipikusan nem tesszük kívülről elérhetővé, ezért azok elérését, módosítását tulajdonságokon keresztül biztosítjuk
        // get és set kulcsszavakkal jelölt speciális hozzáférési metódusokból állhat
        // - a get az olvasáskor, a set az íráskor lefutó műveleteket tartalmazza
        // - a set vagy a get metódus elhagyható, ebben az esetben az adott tulajdonságot nem tudjuk írni vagy olvasni
        // - a get és set láthatósági jelzője eltérhet
        // - előnye: az olvasás és írás jogot külön adhatjuk meg

        // másképp fogalmazva: a tulajdonság egy hozzáférést szabályzó réteg az adattag előtt

        // Az adatrejtés elve szerint mindig csak azoknak az adattagoknak szélesítjük a hozzáférhetőségét, amelyek megkövetelik.
        // FIGYELEM: Amennyiben egy adattag indokolatlanul publikus, vagy indokolatlanul készül tulajdonság hozzá, HIBÁNAK számít!




        // privát adattag
        private string neptunkod;

        // publikus hozzáférést segítő metódusok
        public string GetNeptunkod()
        {
            return neptunkod;
        }

        public void SetNeptunkod(string value)
        {
            if (Ervenyes(value))
                neptunkod = value;
        }

        // publikus hozzáférést segítő tulajdonság
        public string Neptunkod // a tulajdonság elnevezése gyakran az adatmező nevének nagy kezdőbetűs változata
        {
            get  // a get egy speciális kiolvasó metódus
            {
                return neptunkod;
            }
            set  // a set egy speciális író metódus
            {
                if (Ervenyes(value)) // adat ellenőrzésése kifejezetten alkalmas a set blokk
                    neptunkod = value; // value: az átadott érték (rejtett paraméter)
            }
        }



        private string vezeteknev;
        private string keresztnev;

        // önálló tulajdonság
        public string Nev
        {
            get { return vezeteknev + " " + keresztnev; } // nincs set metódus --> csak olvasható tulajdonság
        }



        private int szuletesiEv;

        public int Eletkor { get { return 2019 - szuletesiEv; } } // csak olvasható tulajdonság



        // automatikus tulajdonság
        public bool AktivStatusz { get; set; }
        // - getter és setter blokkokban nincs lehetőség kód írására
        // - el is hagyható például a setter blokk (ebben az esetben a tulajdonság csak a konstruktorban kaphat értéket)



        // a get és set láthatósági jelzője eltérhet
        public int OsztondijIndex { get; private set; }
        // ebben az esetben a tulajdonság osztályon belülről írható és olvasható, kívülről (másik osztályokból) csak olvasható



        private bool Ervenyes(string neptunkod)
        {
            if (neptunkod.Length != 6)
                return false;
            else
            {
                int j = 0;
                while (j < neptunkod.Length && char.IsLetterOrDigit(neptunkod[j]))
                {
                    j++;
                }
                //if (j == neptunkod.Length)
                //    return true;
                //return false;
                return j == neptunkod.Length;
            }
        }


        /*~Hallgato()
        {
            // destruktor
        }*/
    }
}

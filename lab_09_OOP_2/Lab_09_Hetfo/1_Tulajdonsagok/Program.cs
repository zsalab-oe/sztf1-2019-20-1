﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _1_Tulajdonsagok
{
    class Program
    {
        static void Main(string[] args)
        {
            Hallgato pista = new Hallgato();

            // értékadás, érték lekérdezése metódusokon keresztül
            pista.SetNeptunkod("ABC123");
            Console.WriteLine(pista.GetNeptunkod());

            // értékadás, érték lekérdezése tulajdonságon keresztül
            pista.Neptunkod = "QWE789";
            Console.WriteLine(pista.Neptunkod);

            pista.AktivStatusz = true;
            //pista.OsztondijIndex = 3000;

            // comment:     Ctrl + K + C
            // uncomment:   Ctrl + K + U


            // a publikus és írható tulajdonságoknak a konstruktorhívás után az alábbi kódsorral is adhatunk értéket
            // FONTOS: az értékadások a konstruktor lefutása UTÁN történnek
            Hallgato bela = new Hallgato() { Neptunkod = "BELA01", AktivStatusz = true };

            Console.WriteLine("==============");

            /* objektumtömb */

            Hallgato[] hallgatok = new Hallgato[]
                {
                    new Hallgato() {Neptunkod = "AAA111", AktivStatusz = true },
                    new Hallgato() {Neptunkod = "BBB222", AktivStatusz = true },
                    new Hallgato() {Neptunkod = "CCC333", AktivStatusz = true },
                    new Hallgato() {Neptunkod = "DDD444", AktivStatusz = true },
                    new Hallgato() {Neptunkod = "EEE555", AktivStatusz = true }
                };

            // véletlenszerűen megváltoztatjuk néhány hallgató aktív státuszát
            Random rnd = new Random();
            for (int i = 0; i < hallgatok.Length; i++)
            {
                if (rnd.Next(0, 10) < 4)
                    hallgatok[i].AktivStatusz = false;
            }

            // töröljük a tömbből a nem aktív státuszú hallgatókat
            for (int i = 0; i < hallgatok.Length; i++)
            {
                if (!hallgatok[i].AktivStatusz)
                {
                    Console.WriteLine("Viszlát, " + hallgatok[i].Neptunkod);
                    hallgatok[i] = null; // törlés megvalósítása: levesszük a hivatkozást az adott objektumról
                }
            }

            Console.WriteLine("==============");

            for (int i = 0; i < hallgatok.Length; i++)
            {
                // előfordulhat, hogy a tömb egyes elemeinek null az értéke (törölt hallgatók helyén)
                // null elemre nem lehet hivatkozni
                // ezért hozzáférés előtt ellenőrizzük, hogy érvényes memóriacímre hivatkozik-e a tömb i. eleme
                if (hallgatok[i] != null)
                    Console.WriteLine(hallgatok[i].Neptunkod + ", " + hallgatok[i].AktivStatusz);
            }


            Console.WriteLine("\n>> Felvétel");
            Hallgato ujHallgato = new Hallgato() { Neptunkod = "XXX000" };

            // megpróbálunk felvenni egy új hallgatót egy üres helyre (ha van)
            if (Felvetel(ujHallgato, hallgatok))
            {
                for (int i = 0; i < hallgatok.Length; i++)
                {
                    if (hallgatok[i] != null)
                        Console.WriteLine(hallgatok[i].Neptunkod + ", " + hallgatok[i].AktivStatusz);
                }
            }


            Console.WriteLine("\n==============");
            // tömb VS tömbelem kiírása
            int[] tomb = new int[4];
            Console.WriteLine(tomb);
            Console.WriteLine(tomb[0]);




            Console.ReadLine();
        }

        static bool Felvetel(Hallgato ujHallgato, Hallgato[] hallgatok)
        {
            int j = 0;
            while (j < hallgatok.Length && hallgatok[j] != null) // megkeressük az első üres helyet a tömbben
            {
                j++;
            }
            if (j < hallgatok.Length)
            {
                hallgatok[j] = ujHallgato;
                ujHallgato.AktivStatusz = true;
                return true; // a felvétel sikerült
            }
            return false; // a felvétel nem sikerült
        }
    }
}

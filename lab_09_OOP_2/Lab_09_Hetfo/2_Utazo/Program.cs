﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2_Utazo
{
    class Program
    {
        static void Main(string[] args)
        {
            Utazo vandor = new Utazo();
            vandor.Utazik("Budapest");
            vandor.Utazik("Bécs");
            vandor.Utazik("Róma");
            vandor.Utazik("London");
            vandor.Utazik("Budapest");
            vandor.Utazik("Róma");

            Console.WriteLine("A vándor az alábbi helyeken járt:");
            TombKiir(vandor.HelyekTombkent);

            Console.WriteLine("\n=========== Feladatok ===========\n");

            Console.WriteLine("Járt Bécsben? " + vandor.JartE("Bécs"));
            Console.WriteLine("Járt New Yorkban? " + vandor.JartE("New York"));
            Console.WriteLine("A vándor eddig összesen {0} helyen járt.", vandor.HanyHelyenJart());
            Console.WriteLine("A vándor eddig összesen {0} helyen járt.", vandor.HanyHelyenJartProperty);
            Console.WriteLine("Hol járt előbb, Bécsben vagy Londonban? " + vandor.HolVoltElobb("Bécs", "London"));
            Console.WriteLine("Hol járt előbb, Bécsben vagy Budapesten? " + vandor.HolVoltElobb("Bécs", "Budapest"));
            Console.WriteLine("Hol járt előbb, New Yorkban vagy Londonban? " + vandor.HolVoltElobb("New York", "London"));
            Console.WriteLine("Hol járt előbb, Kairóban vagy New Yorkban? " + vandor.HolVoltElobb("Kairó", "New York"));

            Console.WriteLine("\nHelyek, ahol járt:");
            string[] egyediHelyek = vandor.HelyekIsmetlodesNelkul();
            TombKiir(egyediHelyek);

            Console.WriteLine("\nHelyek, ahol többször járt:");
            TombKiir(vandor.HelyekAholTobbszorJart());

            Console.ReadLine();
        }

        static void TombKiir(string[] sTomb)
        {
            for (int i = 0; i < sTomb.Length; i++)
            {
                Console.WriteLine(sTomb[i]);
            }
        }
    }
}
